<?php
// ---- COMMON ----
$lang['Common.COMM-0001'] = 'Sample message';

// ---- MASTER PRINCIPAL--
$lang['Principal.PRCP-0001'] = 'Principal successfully inserted.';
$lang['Principal.PRCP-0002'] = 'Principal successfully updated.';
$lang['Principal.PRCP-0003'] = 'Principal successfully deleted.';