<?php
/**
 * Created by PhpStorm.
 * User: toni
 * Date: 30/03/2016
 * Time: 6:16 PM
 */
// ---------------- GLOBAL ----------------------------
$lang['remaining_chars']                        = "Remaining Chars ";
$lang['save']                                   = "Save";
$lang['update']                                 = "Update";
$lang['delete']                                 = "Delete";
$lang['cancel']                                 = "Cancel";
$lang['back']                                   = "Back";
$lang['search']                                 = "Search";
$lang['create_new']                             = "Create New";
$lang['clear']                                  = "Clear";

// ---------------- MASTER PRINCIPAL ------------------
$lang['principal']                              = "Principal";
$lang['principal_header']                       = "Master Principal";
$lang['principal_search']                       = "Search Principal";
$lang['principal_code']                         = "Code";
$lang['principal_name']                         = "Name";
$lang['principal_address']                      = "Address";
$lang['principal_city']                         = "City";
$lang['principal_postcode']                     = "Post Code";
$lang['principal_pic']                          = "PIC";
$lang['principal_telp']                         = "Telephone";
$lang['principal_npwp']                         = "Tax No";
$lang['principal_npwp_name']                    = "Tax Holder Name";
$lang['principal_npwp_address']                 = "Tax Holder Address";

// ---------------- MASTER BARANG/ARTIKEl ---------------------
$lang['arcticle']                               = "Barang/Artikel";
$lang['article_header']                         = "Master Barang/Artikel";
$lang['article_code']                           = "Kode";
$lang['article_name']                           = "Nama";
