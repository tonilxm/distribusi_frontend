<?php
// ---- COMMON ----
$lang['Common.COMM-0001'] = 'Sample message';
$lang['Common.COMM-0002'] = 'Anda yakin ingin menghapus data ini?';
$lang['Common.COMM-0003'] = 'Maaf, Terjadi error saat penyimpanan data.';
$lang['Common.COMM-0004'] = 'Maaf, Terjadi error saat penghapusan data.';

// ---- MASTER PRINCIPAL--
$lang['Principal.PRCP-0001'] = 'Data prinsipal sukses tersimpan.';
$lang['Principal.PRCP-0002'] = 'Data prinsipal sukses diubah.';
$lang['Principal.PRCP-0003'] = 'Data prinsipal sukses dihapus.';

// ---- MASTER AREA--
$lang['Area.AREA-0001'] = 'Data area sukses tersimpan.';
$lang['Area.AREA-0002'] = 'Data area sukses diubah.';
$lang['Area.AREA-0003'] = 'Data area sukses dihapus.';

// ---- MASTER OUTLET TYPE--
$lang['OutletType.OLTP-0001'] = 'Data tipe outlet sukses tersimpan.';
$lang['OutletType.OLTP-0002'] = 'Data tipe outlet sukses diubah.';
$lang['OutletType.OLTP-0003'] = 'Data tipe outlet sukses dihapus.';

// ---- MASTER OUTLET--
$lang['Outlet.OUTL-0001'] = 'Data outlet sukses tersimpan.';
$lang['Outlet.OUTL-0002'] = 'Data outlet sukses diubah.';
$lang['Outlet.OUTL-0003'] = 'Data outlet sukses dihapus.';

// ---- MASTER SALESMAN--
$lang['Salesman.SLSM-0001'] = 'Data salesman sukses tersimpan.';
$lang['Salesman.SLSM-0002'] = 'Data salesman sukses diubah.';
$lang['Salesman.SLSM-0003'] = 'Data salesman sukses dihapus.';

// ---- MASTER BRAND--
$lang['Brand.BRND-0001'] = 'Data brand sukses tersimpan.';
$lang['Brand.BRND-0002'] = 'Data brand sukses diubah.';
$lang['Brand.BRND-0003'] = 'Data brand sukses dihapus.';