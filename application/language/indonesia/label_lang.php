<?php
/**
 * Created by PhpStorm.
 * User: toni
 * Date: 30/03/2016
 * Time: 6:16 PM
 */
// ---------------- GLOBAL ----------------------------
$lang['remaining_chars']                        = "Sisa karakter ";
$lang['save']                                   = "Simpan";
$lang['update']                                 = "Ubah";
$lang['delete']                                 = "Hapus";
$lang['cancel']                                 = "Batal";
$lang['back']                                   = "Kembali";
$lang['search']                                   = "Cari";
$lang['create_new']                                   = "Buat Baru";
$lang['clear']                                   = "Hapus Semua";
$lang['ok']                                       = "Ok";
$lang['confirm_delete']                           = "Konfirmasi Hapus";
$lang['select_city']                            = "Pilih Kota";
$lang['select_outlet_area']                            = "Pilih Outlet Area";
$lang['select_outlet_type']                            = "Pilih Outlet Tipe";

// ---------------- MASTER PRINCIPAL ------------------
$lang['principal']                              = "Prinsipal";
$lang['principal_new']                          = "Buat Prinsipal Baru";
$lang['principal_search']                       = "Cari Prinsipal";
$lang['principal_edit']                          = "Ubah Prinsipal";
$lang['principal_code']                         = "Kode";
$lang['principal_name']                         = "Nama";
$lang['principal_address']                      = "Alamat";
$lang['principal_city']                         = "Kota";
$lang['principal_postcode']                     = "Kode Post";
$lang['principal_pic']                          = "PIC";
$lang['principal_telp']                         = "Telephone";
$lang['principal_npwp']                         = "NPWP";
$lang['principal_npwp_name']                    = "Nama Wajib Pajak";
$lang['principal_npwp_address']                 = "Alamat Wajib Pajak";
$lang['principal_npwp_address']                 = "Alamat Wajib Pajak";


// ---------------- MASTER AREA/DISTRICT ---------------------
$lang['area']                                   = "Area";
$lang['area_new']                         = "Buat Area Baru";
$lang['area_edit']                         = "Ubah Area";
$lang['area_search']                       = "Cari Area";
$lang['area_code']                           = "Kode";
$lang['area_name']                           = "Nama";

// ---------------- MASTER OUTLET TYPE ---------------------
$lang['outletType']                                   = "Tipe Outlet";
$lang['outletType_new']                         = "Buat Tipe Outlet Baru";
$lang['outletType_edit']                         = "Ubah Tipe Outlet";
$lang['outletType_search']                       = "Cari Tipe Outlet";
$lang['outletType_code']                           = "Kode";
$lang['outletType_name']                           = "Nama";

// ---------------- MASTER BARANG/ARTIKEl ---------------------
$lang['arcticle']                               = "Barang/Artikel";
$lang['article_header']                         = "Master Barang/Artikel";
$lang['article_code']                           = "Kode";
$lang['article_name']                           = "Nama";

// ---------------- MASTER OUTLET ------------------
$lang['outlet']                              = "Outlet";
$lang['outlet_new']                          = "Buat Outlet Baru";
$lang['outlet_search']                       = "Cari Outlet";
$lang['outlet_edit']                          = "Ubah Outlet";
$lang['outlet_code']                         = "Kode";
$lang['outlet_name']                         = "Nama";
$lang['outlet_address']                      = "Alamat";
$lang['outlet_area']                         = "Area";
$lang['outlet_type']                         = "Type";
$lang['outlet_postcode']                     = "Kode Post";
$lang['outlet_pic']                          = "PIC";
$lang['outlet_telp']                         = "Telephone";
$lang['outlet_npwp']                         = "NPWP";
$lang['outlet_npwp_name']                    = "Nama Wajib Pajak";
$lang['outlet_npwp_address']                 = "Alamat Wajib Pajak";
$lang['outlet_main_data']                    = "Data Utama";
$lang['outlet_bank_data']                    = "Data Bank";
$lang['outlet_bank_name']                    = "Bank";
$lang['outlet_bank_acc_no']                  = "Nomor Rekening";

// ---------------- MASTER SALESMAN ---------------------
$lang['salesman']                             = "Salesman";
$lang['salesman_new']                         = "Buat Salesman Baru";
$lang['salesman_edit']                        = "Ubah Salesman";
$lang['salesman_search']                      = "Cari Salesman";
$lang['salesman_code']                        = "Kode";
$lang['salesman_name']                        = "Nama";
$lang['salesman_address']                     = "Alamat";
$lang['salesman_telp']                        = "Telephone";

// ---------------- MASTER BRAND ---------------------
$lang['brand']                                = "Merk";
$lang['brand_new']                            = "Buat Merk Baru";
$lang['brand_edit']                           = "Ubah Merk";
$lang['brand_search']                         = "Cari Merk";
$lang['brand_code']                           = "Kode";
$lang['brand_name']                           = "Nama";
$lang['brand_principal']                      = "Prinsipal";