<?php $this->load->view('theme/_inc/head'); ?>
<body>
<?php $this->load->view('theme/_inc/notice'); ?>
<?php $this->load->view('theme/_inc/header'); ?>
<div class="container-fluid">
  <div class="row">
      <?= $view_content ?>

      <?php $this->load->view('theme/_inc/side_nav'); ?>

  </div>
</div>
<?php $this->load->view('theme/_inc/footer'); ?>
</body>
</html>