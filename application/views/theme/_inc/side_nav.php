
<div class="col-sm-3 col-md-2 col-sm-pull-9 col-md-pull-10 sidebar-pf sidebar-pf-left">
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
            Master
          </a>
        </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in">
        <div class="panel-body">
          <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="<?= site_url('principal') ?>"><?= lang("principal") ?></a></li>
            <li><a href="<?= site_url('area') ?>"><?= lang("area") ?></a></li>
            <li><a href="<?= site_url('outletType') ?>"><?= lang("outletType") ?></a></li>
            <li><a href="<?= site_url('outlet') ?>"><?= lang("outlet") ?></a></li>
            <li><a href="<?= site_url('salesman') ?>"><?= lang("salesman") ?></a></li>
            <li><a href="<?= site_url('brand') ?>"><?= lang("brand") ?></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">
            Risus
          </a>
        </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse">
        <div class="panel-body">
          <ul class="nav nav-pills nav-stacked">
            <li><a href="#">Adipiscing elit</a></li>
            <li><a href="#">Duis</a></li>
            <li><a href="#">Pellentesque</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">
            Iaculis quis feugiat
          </a>
        </h4>
      </div>
      <div id="collapseThree" class="panel-collapse collapse">
        <div class="panel-body">
          <ul class="nav nav-pills nav-stacked">
            <li><a href="#">Sed est</a></li>
            <li><a href="#">Curabitur</a></li>
            <li><a href="#">Eu dignissim</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
