<nav class="navbar navbar-default navbar-pf" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/">
			<img src="/assets/app/img/brand.svg" alt="PatternFly Enterprise Application" />
		</a>
	</div>
	<div class="collapse navbar-collapse navbar-collapse-1">
		<ul class="nav navbar-nav navbar-utility">
			<li>
				<a href="#">Status</a>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<span class="pficon pficon-user"></span>
					Brian Johnson <b class="caret"></b>
				</a>
				<ul class="dropdown-menu">
					<li>
						<a href="#">Link</a>
					</li>
					<li>
						<a href="#">Another link</a>
					</li>
					<li>
						<a href="#">Something else here</a>
					</li>
					<li class="divider"></li>
					<li class="dropdown-submenu">
						<a tabindex="-1" href="#">More options</a>
						<ul class="dropdown-menu">
							<li>
								<a href="#">Link</a>
							</li>
							<li>
								<a href="#">Another link</a>
							</li>
							<li>
								<a href="#">Something else here</a>
							</li>
							<li class="divider"></li>
							<li class="dropdown-header">Nav header</li>
							<li>
								<a href="#">Separated link</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="#">One more separated link</a>
							</li>
						</ul>
					</li>
					<li class="divider"></li>
					<li>
						<a href="#">One more separated link</a>
					</li>
				</ul>
			</li>
		</ul>
		<!--
		<ul class="nav navbar-nav navbar-primary">
			<li>
				<a href="basic.html">Basic</a>
			</li>
			<li>
				<a href="bootstrap-treeview-2.html">Tree View</a>
			</li>
			<li>
				<a href="dashboard.html">Dashboard</a>
			</li>
			<li class="active">
				<a href="form.html" class="active">Form</a>
			</li>
			<li>
				<a href="tab.html">Tab</a>
			</li>
			<li>
				<a href="typography-2.html">Typography</a>
			</li>
			<li>
				<a href="cards.html">Cards</a>
			</li>
		</ul>
		-->
	</div>
</nav>
