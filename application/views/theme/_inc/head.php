<!DOCTYPE html>
<!--[if IE 9]><html lang="en-us" class="ie9 login-pf"><![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en-us" class="<?php echo $this->router->fetch_method() == 'login'?'login-pf':$this->router->fetch_method()."-pf" ?>">
<!--<![endif]-->
<head>
  <title>Login - PatternFly</title>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="/assets/vendor/patternfly/img/favicon.ico">
  <!-- iPad retina icon -->
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/assets/vendor/patternfly/img/apple-touch-icon-precomposed-152.png">
  <!-- iPad retina icon (iOS < 7) -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/vendor/patternfly/img/apple-touch-icon-precomposed-144.png">
  <!-- iPad non-retina icon -->
  <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/assets/vendor/patternfly/img/apple-touch-icon-precomposed-76.png">
  <!-- iPad non-retina icon (iOS < 7) -->
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/vendor/patternfly/img/apple-touch-icon-precomposed-72.png">
  <!-- iPhone 6 Plus icon -->
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/assets/vendor/patternfly/img/apple-touch-icon-precomposed-180.png">
  <!-- iPhone retina icon (iOS < 7) -->
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/vendor/patternfly/img/apple-touch-icon-precomposed-114.png">
  <!-- iPhone non-retina icon (iOS < 7) -->
  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/assets/vendor/patternfly/img/apple-touch-icon-precomposed-57.png">
  <link rel="stylesheet" href="/assets/vendor/patternfly/css/patternfly.min.css" >
  <link rel="stylesheet" href="/assets/vendor/patternfly/css/patternfly-additions.min.css" >

  <link rel="stylesheet" href="/assets/vendor/bootstrap-combobox/css/bootstrap-combobox.css" >

  <!--  Application CSS -->
  <link rel="stylesheet" href="/assets/app/css/app.css" >

  <!-- injected css from controller -->
  <?php if (isset($stylesheets))
      {
        foreach ($stylesheets as $stylesheet)
        {
          echo "<link rel=\"stylesheet\" href=\"/assets/app/css/". $stylesheet. ".css\">";
        }
      }
  ?>

  <!-- 3rd party script -->
  <!--<script src="/assets/vendor/richmarker.js"></script>-->
  <script src="/assets/vendor/markerclusterer.js"></script>
  <script src="/assets/vendor/jquery2.1.4/jquery.min.js"></script>
  <script src="/assets/vendor/bootstrap3.3.6/js/bootstrap.min.js"></script>
  <script src="/assets/vendor/datatable.net/jquery.dataTables.js"></script>
  <script src="/assets/vendor/patternfly/js/patternfly.min.js"></script>
  <script src="/assets/vendor/modernizr-2.6.2.min.js"></script>
  <script src="/assets/vendor/bootstrap-combobox/js/bootstrap-combobox.js"></script>
</head>