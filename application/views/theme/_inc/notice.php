<?php if ($notice && is_array($notice)) : ?>
<div class="toast-pf toast-pf-max-width toast-pf-top-right alert alert-warning alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
        <span class="pficon pficon-close"></span>
    </button>
    <?php if ($notice['type'] == 'warning') : ?>
        <span class="pficon pficon-warning-triangle-o"></span>
    <?php endif; ?>
    <?php if ($notice['type'] == 'info') : ?>
        <span class="pficon pficon-info" style="background-color: green;"></span>
    <?php endif; ?>
    <?php echo $notice['message']; ?>
</div>
<?php endif; ?>