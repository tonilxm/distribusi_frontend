<div class="col-sm-9 col-md-10 col-sm-push-3 col-md-push-2">
  <h1><?= lang("area_new") ?></h1>
  <hr>
  <form class="form-horizontal" method="post" action="<?= site_url('area/save') ?>">
    <div class="form-group">
      <label class="col-md-2 control-label" for="area_code"><?= lang('area_code') ?></label>
      <div class="col-md-6">
        <input type="text" id="area_code" name="area_code" class="form-control"
               value="<?= isset($area) ? $area->code : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="area_name"><?= lang('area_name') ?></label>
      <div class="col-md-6">
        <input type="text" id="area_name" name="area_name" class="form-control"
               value="<?= isset($area) ? $area->name : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-10 col-md-offset-2">
        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
        <a href="<?= site_url('area') ?>" class="btn btn-default"><?= lang('cancel') ?></a>
      </div>
    </div>
  </form>
</div>