<div class="col-sm-9 col-md-10 col-sm-push-3 col-md-push-2">
  <h1><?= lang('area_search') ?></h1>
  <hr>
  <div class="row">
    <div class="col-sm-12">
      <div class="col-sm-6">
        <label class="col-sm-2 control-label" for="area_code"><?= lang('area_code') ?></label>
        <div class="col-sm-8">
          <input type="text" id="area_code" name="area_code" class="form-control">
        </div>
      </div>
      <div class="col-sm-6">
        <label class="col-sm-2 control-label" for="area_name"><?= lang('area_name') ?></label>
        <div class="col-sm-8">
          <input type="text" id="area_name" name="area_name" class="form-control">
        </div>
      </div>
      <hr>
      <div class="col-sm-12">
        <button type="button" id="btnSearch" class="btn btn-default"><?= lang("search") ?></button>
        <a class="btn btn-primary" href="area/new" role="button"><?= lang("create_new") ?></a>
        <button type="button" class="btn btn-danger" id="btnClearAll"><?= lang("clear") ?></button>
      </div>
      <p></p>
      <div class="col-sm-12">
        <div class="row">
          <table class="datatable table table-striped table-bordered">
            <thead>
            <tr>
              <th><?= lang('area_code') ?></th>
              <th><?= lang('area_name') ?></th>
            </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>