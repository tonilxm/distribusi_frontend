<div class="col-sm-9 col-md-10 col-sm-push-3 col-md-push-2">
  <h1><?= lang("brand_edit") ?></h1>
  <hr>
  <div class="row">
    <div class="col-sm-12">
      <form class="form-horizontal" method="post" action="<?= site_url('brand/update') ?>">
        <input type="hidden" name="brand_id" value="<?= isset($brand) ? $brand->id : '' ?>">
        <div class="form-group">
          <label class="col-md-2 control-label" for="brand_code"><?= lang('brand_code') ?></label>
          <div class="col-md-6">
            <input type="text" id="brand_code" name="brand_code" class="form-control"
                   value="<?= isset($brand) ? $brand->code : '' ?>" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label" for="brand_name"><?= lang('brand_name') ?></label>
          <div class="col-md-6">
            <input type="text" id="brand_name" name="brand_name" class="form-control"
                   value="<?= isset($brand) ? $brand->name : '' ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label" for="brand_principal"><?= lang('brand_principal') ?></label>
          <div class="col-md-6">
            <select class="combobox form-control" id="brand_principal_id" name="brand_principal_id">
              <option value="" selected="selected"><?= lang('select_brand_principal') ?></option>
              <?php foreach ($principals as $principal): ?>
                <option value="<?= $principal->id ?>" <?php if (isset($brand) && $brand->principal->id == $principal->id) {
                  echo "selected";
                } ?>><?= $principal->name ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-10 col-md-offset-2">
            <button type="submit" class="btn btn-primary update"><?= lang('update') ?></button>
            <button class="btn btn-danger delete" data-toggle="modal" data-target="#confirmDeleteModal" id="btnDelete">
              <?= lang('delete') ?>
            </button>
            <a href="<?= site_url('brand') ?>" class="btn btn-default"><?= lang('back') ?></a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="confirmDeleteModal" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteModalLabel"
     aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <span class="pficon pficon-close"></span>
        </button>
        <h4 class="modal-title" id="confirmDeleteModalLabel"><?= lang('confirm_delete') ?></h4>
      </div>
      <div class="modal-body">
        <h4 class="center-block"><?= lang('Common.COMM-0002') ?></h4>
      </div>
      <div class="modal-footer">
        <a href="<?= site_url('brand/' . $brand->id . '/delete') ?>" class="btn btn-primary" id="btnDelete">
          <?= lang('ok') ?>
        </a>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
      </div>
    </div>
  </div>
</div>