<div class="col-sm-9 col-md-10 col-sm-push-3 col-md-push-2">
  <h1><?= lang("brand_new") ?></h1>
  <hr>
  <form class="form-horizontal" method="post" action="<?= site_url('brand/save') ?>">
    <div class="form-group">
      <label class="col-md-2 control-label" for="brand_code"><?= lang('brand_code') ?></label>
      <div class="col-md-6">
        <input type="text" id="brand_code" name="brand_code" class="form-control"
               value="<?= isset($brand) ? $brand->code : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="brand_name"><?= lang('brand_name') ?></label>
      <div class="col-md-6">
        <input type="text" id="brand_name" name="brand_name" class="form-control"
               value="<?= isset($brand) ? $brand->name : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="brand_principal"><?= lang('brand_principal') ?></label>
      <div class="col-md-6">
        <select class="combobox form-control" id="brand_principal_id" name="brand_principal_id">
          <option value="" selected="selected"><?= lang('select_brand_principal') ?></option>
          <?php foreach ($principals as $principal): ?>
            <option value="<?= $principal->id ?>" <?php if (isset($brand) && $brand->principal->id == $principal->id) {
              echo "selected";
            } ?>><?= $principal->name ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-10 col-md-offset-2">
        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
        <a href="<?= site_url('brand') ?>" class="btn btn-default"><?= lang('cancel') ?></a>
      </div>
    </div>
  </form>
</div>