<div class="col-sm-9 col-md-10 col-sm-push-3 col-md-push-2">
  <h1><?= lang("outlet_new") ?></h1>
  <hr>
  <ul class="nav nav-tabs">
    <li class="active"><a href="#" id="main_data_lnk"><?= lang("outlet_main_data") ?></a></li>
    <li><a href="#" id="bank_data_lnk"><?= lang("outlet_bank_data") ?></a></li>
  </ul>
  <p></p>
  <form class="form-horizontal" method="post" action="<?= site_url('outlet/save') ?>" id="main_form">
    <div class="main-data">
      <div class="form-group">
        <label class="col-md-2 control-label" for="outlet_code"><?= lang('outlet_code') ?></label>
        <div class="col-md-6">
          <input type="text" id="outlet_code" name="outlet_code" class="form-control"
                 value="<?= isset($outlet) ? $outlet->code : '' ?>">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-2 control-label" for="outlet_name"><?= lang('outlet_name') ?></label>
        <div class="col-md-6">
          <input type="text" id="outlet_name" name="outlet_name" class="form-control"
                 value="<?= isset($outlet) ? $outlet->name : '' ?>">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-2 control-label" for="outlet_address"><?= lang("outlet_address") ?></label>
        <div class="col-md-6">
      <textarea class="form-control" id="outlet_address" name="outlet_address"
                placeholder="Type in your message"
                rows="3"><?= isset($outlet) ? $outlet->address : '' ?></textarea>
      <span class="pull-right chars-remaining-pf">
          <?= lang("remaining_chars") ?><span id="charRemainingCntFld"></span>
      </span>
          <script>
            $(function () {
              // countFld is the id of the field where you want the 'remaining chars. count' number
              // to be displayed.
              // $("#pricipal_address").countRemainingChars( {countFld: "charRemainingCntFld"} );

              // all settings/options
              $('#outlet_address').countRemainingChars({
                countFld: 'charRemainingCntFld',
                charsMaxLimit: 150,
                charsWarnRemaining: 5,
                blockInputAtMaxLimit: true
              });


              // taId is the id of the textArea field which triggered the event
              // Helpful if counting remaining chars on multiple TAs
              $('#outlet_address').on("overCharsMaxLimitEvent", function (event, taId) {
                //$('#postBtn').prop("disabled",true);
              });
              $('#outlet_address').on("underCharsMaxLimitEvent", function (event, taId) {
                //$('#postBtn').prop("disabled",false);
              });
            });
          </script>
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-2 control-label" for="outlet_area_id"><?= lang("outlet_area") ?></label>
        <div class="col-md-6">
          <select class="combobox form-control" id="outlet_area_id" name="outlet_area_id">
            <option value="" selected="selected"><?= lang('select_outlet_area') ?></option>
            <?php foreach ($outletAreas as $area): ?>
              <option value="<?= $area->id ?>" <?php if (isset($outlet) && $outlet->areaId == $area->id) {
                echo "selected";
              } ?>><?= $area->name ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-2 control-label" for="outlet_type_id"><?= lang("outlet_type") ?></label>
        <div class="col-md-6">
          <select class="combobox form-control" id="outlet_type_id" name="outlet_type_id">
            <option value="" selected="selected"><?= lang('select_outlet_type') ?></option>
            <?php foreach ($outletTypes as $type): ?>
              <option value="<?= $type->id ?>" <?php if (isset($outlet) && $outlet->typeId == $type->id) {
                echo "selected";
              } ?>><?= $type->name ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-2 control-label" for="outlet_name"><?= lang('outlet_telp') ?></label>
        <div class="col-md-6">
          <input type="text" id="outlet_telp" name="outlet_telp" class="form-control"
                 value="<?= isset($outlet) ? $outlet->telp : '' ?>">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-2 control-label" for="outlet_name"><?= lang('outlet_npwp') ?></label>
        <div class="col-md-6">
          <input type="text" id="outlet_npwp" name="outlet_npwp" class="form-control"
                 value="<?= isset($outlet) ? $outlet->npwp : '' ?>">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-2 control-label" for="outlet_name"><?= lang('outlet_npwp_name') ?></label>
        <div class="col-md-6">
          <input type="text" id="outlet_npwp_name" name="outlet_npwp_name" class="form-control"
                 value="<?= isset($outlet) ? $outlet->npwp_name : '' ?>">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-2 control-label" for="outlet_name"><?= lang('outlet_npwp_address') ?></label>
        <div class="col-md-6">
          <input type="text" id="outlet_npwp_address" name="outlet_npwp_address" class="form-control"
                 value="<?= isset($outlet) ? $outlet->npwp_address : '' ?>">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-2 control-label" for="outlet_name"><?= lang('outlet_pic') ?></label>
        <div class="col-md-6">
          <input type="text" id="outlet_pic" name="outlet_pic" class="form-control"
                 value="<?= isset($outlet) ? $outlet->pic : '' ?>">
        </div>
      </div>
    </div>
    <div class="bank-data" style="display: none">
      <input type="hidden" name="bank_accs[][]" id="bank_accs">
      <div>
        <a href="#" class="btn btn-default" id="btn_add_bank">Tambah Rekening</a>
      </div>
      <p></p>
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover" id="tbl_bank">
          <thead>
          <tr>
            <th style="width: 1.5rem;">No</th>
            <th><?= lang('outlet_bank_acc_no') ?></th>
            <th><?= lang('outlet_bank_name') ?></th>
            <th style="width: 5em;"></th>
          </tr>
          </thead>
          <tbody>
          <?php if(isset($outlet) && !empty($outlet->outletBankAccountList)): ?>
            <?php $row_no = 1; ?>
            <?php foreach($outlet->outletBankAccountList as $bankAcc): ?>
              <tr>
                <td class="text-center row-no"><?= $row_no++ ?> </td>
                <td class="bank-acc-no"><input type="text" name="acc_no[]" value="<?= $bankAcc->accountNo ?>" size="30"></td>
                <td class="bank-info"><input type="text" name="bank_info[]" value="<?= $bankAcc->bankInfo ?>" size="100"></td>
                <td><a href="#" class="btn btn-danger btn-xs remove-bank-row">Hapus</a></td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="col-md-10 col-md-offset-2">
      <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
      <a href="<?= site_url('outlet') ?>" class="btn btn-default"><?= lang('cancel') ?></a>
    </div>
  </form>
</div>
