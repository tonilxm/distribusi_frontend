<div class="col-sm-9 col-md-10 col-sm-push-3 col-md-push-2">
  <h1><?= lang('outlet_search') ?></h1>
  <hr>
  <div class="row">
    <div class="col-sm-12">
      <div class="col-sm-6">
        <label class="col-sm-2 control-label" for="outlet_name"><?= lang('outlet_name') ?></label>
        <div class="col-sm-8">
          <input type="text" id="outlet_name" name="outlet_name" class="form-control">
        </div>
      </div>
      <div class="col-sm-6">
        <label class="col-sm-2 control-label" for="outlet_address"><?= lang('outlet_address') ?></label>
        <div class="col-sm-8">
          <input type="text" id="outlet_address" name="outlet_address" class="form-control">
        </div>
      </div>
      <hr>
      <div class="col-sm-12">
        <button type="button" id="btnSearch" class="btn btn-default"><?= lang("search") ?></button>
        <a class="btn btn-primary" href="outlet/new" role="button"><?= lang("create_new") ?></a>
        <button type="button" class="btn btn-danger" id="btnClearAll"><?= lang("clear") ?></button>
      </div>
      <p></p>
      <div class="col-sm-12">
        <div class="row">
          <table class="datatable table table-striped table-bordered">
            <thead>
            <tr>
              <th><?= lang('outlet_name') ?></th>
              <th><?= lang('outlet_address') ?></th>
              <th><?= lang('outlet_area') ?></th>
            </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>