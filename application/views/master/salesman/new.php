<div class="col-sm-9 col-md-10 col-sm-push-3 col-md-push-2">
  <h1><?= lang("salesman_new") ?></h1>
  <hr>
  <form class="form-horizontal" method="post" action="<?= site_url('salesman/save') ?>">
    <div class="form-group">
      <label class="col-md-2 control-label" for="salesman_code"><?= lang('salesman_code') ?></label>
      <div class="col-md-6">
        <input type="text" id="salesman_code" name="salesman_code" class="form-control"
               value="<?= isset($salesman) ? $salesman->code : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="salesman_name"><?= lang('salesman_name') ?></label>
      <div class="col-md-6">
        <input type="text" id="salesman_name" name="salesman_name" class="form-control"
               value="<?= isset($salesman) ? $salesman->name : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="salesman_address"><?= lang("salesman_address") ?></label>
      <div class="col-md-6">
      <textarea class="form-control" id="salesman_address" name="salesman_address"
                placeholder="Type in your message"
                rows="3"><?= isset($salesman) ? $salesman->address : '' ?></textarea>
      <span class="pull-right chars-remaining-pf">
          <?= lang("remaining_chars") ?><span id="charRemainingCntFld"></span>
      </span>
        <script>
          $(function () {
            // countFld is the id of the field where you want the 'remaining chars. count' number
            // to be displayed.
            // $("#pricipal_address").countRemainingChars( {countFld: "charRemainingCntFld"} );

            // all settings/options
            $('#salesman_address').countRemainingChars({
              countFld: 'charRemainingCntFld',
              charsMaxLimit: 150,
              charsWarnRemaining: 5,
              blockInputAtMaxLimit: true
            });


            // taId is the id of the textArea field which triggered the event
            // Helpful if counting remaining chars on multiple TAs
            $('#salesman_address').on("overCharsMaxLimitEvent", function (event, taId) {
              //$('#postBtn').prop("disabled",true);
            });
            $('#salesman_address').on("underCharsMaxLimitEvent", function (event, taId) {
              //$('#postBtn').prop("disabled",false);
            });
          });
        </script>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="salesman_telp"><?= lang('salesman_telp') ?></label>
      <div class="col-md-6">
        <input type="text" id="salesman_telp" name="salesman_telp" class="form-control"
               value="<?= isset($salesman) ? $salesman->telp : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-10 col-md-offset-2">
        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
        <a href="<?= site_url('salesman') ?>" class="btn btn-default"><?= lang('cancel') ?></a>
      </div>
    </div>
  </form>
</div>