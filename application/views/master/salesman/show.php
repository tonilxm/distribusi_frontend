<div class="col-sm-9 col-md-10 col-sm-push-3 col-md-push-2">
  <h1><?= lang("salesman_edit") ?></h1>
  <hr>
  <div class="row">
    <div class="col-sm-12">
      <form class="form-horizontal" method="post" action="<?= site_url('salesman/update') ?>">
        <input type="hidden" name="salesman_id" value="<?= isset($salesman) ? $salesman->id : '' ?>">
        <div class="form-group">
          <label class="col-md-2 control-label" for="salesman_code"><?= lang('salesman_code') ?></label>
          <div class="col-md-6">
            <input type="text" id="salesman_code" name="salesman_code" class="form-control"
                   value="<?= isset($salesman) ? $salesman->code : '' ?>" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label" for="salesman_name"><?= lang('salesman_name') ?></label>
          <div class="col-md-6">
            <input type="text" id="salesman_name" name="salesman_name" class="form-control"
                   value="<?= isset($salesman) ? $salesman->name : '' ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label" for="salesman_address"><?= lang("salesman_address") ?></label>
          <div class="col-md-6">
            <textarea class="form-control" id="salesman_address" name="salesman_address"
                      placeholder="Type in your message"
                      rows="3"><?= isset($salesman) ? $salesman->address : '' ?></textarea>
            <span class="pull-right chars-remaining-pf">
                <?= lang("remaining_chars") ?><span id="charRemainingCntFld"></span>
            </span>
            <script>
              $(function () {
                // countFld is the id of the field where you want the 'remaining chars. count' number
                // to be displayed.
                // $("#pricipal_address").countRemainingChars( {countFld: "charRemainingCntFld"} );

                // all settings/options
                $('#salesman_address').countRemainingChars({
                  countFld: 'charRemainingCntFld',
                  charsMaxLimit: 150,
                  charsWarnRemaining: 5,
                  blockInputAtMaxLimit: true
                });


                // taId is the id of the textArea field which triggered the event
                // Helpful if counting remaining chars on multiple TAs
                $('#salesman_address').on("overCharsMaxLimitEvent", function (event, taId) {
                  //$('#postBtn').prop("disabled",true);
                });
                $('#salesman_address').on("underCharsMaxLimitEvent", function (event, taId) {
                  //$('#postBtn').prop("disabled",false);
                });
              });
            </script>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label" for="salesman_telp"><?= lang('salesman_telp') ?></label>
          <div class="col-md-6">
            <input type="text" id="salesman_telp" name="salesman_telp" class="form-control"
                   value="<?= isset($salesman) ? $salesman->telp : '' ?>">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-10 col-md-offset-2">
            <button type="submit" class="btn btn-primary update"><?= lang('update') ?></button>
            <button class="btn btn-danger delete" data-toggle="modal" data-target="#confirmDeleteModal" id="btnDelete">
              <?= lang('delete') ?>
            </button>
            <a href="<?= site_url('salesman') ?>" class="btn btn-default"><?= lang('back') ?></a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="confirmDeleteModal" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteModalLabel"
     aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <span class="pficon pficon-close"></span>
        </button>
        <h4 class="modal-title" id="confirmDeleteModalLabel"><?= lang('confirm_delete') ?></h4>
      </div>
      <div class="modal-body">
        <h4 class="center-block"><?= lang('Common.COMM-0002') ?></h4>
      </div>
      <div class="modal-footer">
        <a href="<?= site_url('salesman/' . $salesman->id . '/delete') ?>" class="btn btn-primary" id="btnDelete">
          <?= lang('ok') ?>
        </a>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
      </div>
    </div>
  </div>
</div>