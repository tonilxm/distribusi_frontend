<div class="col-sm-9 col-md-10 col-sm-push-3 col-md-push-2">
  <h1><?= lang('salesman_search') ?></h1>
  <hr>
  <div class="row">
    <div class="col-sm-12">
      <div class="col-sm-6">
        <label class="col-sm-2 control-label" for="salesman_name"><?= lang('salesman_name') ?></label>
        <div class="col-sm-8">
          <input type="text" id="salesman_name" name="salesman_name" class="form-control">
        </div>
      </div>
      <div class="col-sm-6">
        <label class="col-sm-2 control-label" for="salesman_address"><?= lang('salesman_address') ?></label>
        <div class="col-sm-8">
          <input type="text" id="salesman_address" name="salesman_address" class="form-control">
        </div>
      </div>
      <hr>
      <div class="col-sm-12">
        <button type="button" id="btnSearch" class="btn btn-default"><?= lang("search") ?></button>
        <a class="btn btn-primary" href="salesman/new" role="button"><?= lang("create_new") ?></a>
        <button type="button" class="btn btn-danger" id="btnClearAll"><?= lang("clear") ?></button>
      </div>
      <p></p>
      <div class="col-sm-12">
        <div class="row">
          <table class="datatable table table-striped table-bordered">
            <thead>
            <tr>
              <th><?= lang('salesman_name') ?></th>
              <th><?= lang('salesman_address') ?></th>
              <th><?= lang('salesman_telp') ?></th>
            </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>