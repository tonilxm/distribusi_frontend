<div class="col-sm-9 col-md-10 col-sm-push-3 col-md-push-2">
  <h1><?= lang('principal_search') ?></h1>
  <hr>
  <div class="row">
    <div class="col-sm-12">
      <div class="col-sm-6">
        <label class="col-sm-2 control-label" for="principal_name"><?= lang('principal_name') ?></label>
        <div class="col-sm-8">
          <input type="text" id="principal_name" name="principal_name" class="form-control">
        </div>
      </div>
      <div class="col-sm-6">
        <label class="col-sm-2 control-label" for="principal_city_address"><?= lang('principal_address') ?></label>
        <div class="col-sm-8">
          <input type="text" id="principal_city_address" name="principal_address" class="form-control">
        </div>
      </div>
      <hr>
      <div class="col-sm-12">
        <button type="button" id="btnSearch" class="btn btn-default"><?= lang("search") ?></button>
        <a class="btn btn-primary" href="principal/new" role="button"><?= lang("create_new") ?></a>
        <button type="button" class="btn btn-danger" id="btnClearAll"><?= lang("clear") ?></button>
      </div>
      <p></p>
      <div class="col-sm-12">
        <div class="row">
          <table class="datatable table table-striped table-bordered">
            <thead>
            <tr>
              <th><?= lang('principal_name') ?></th>
              <th><?= lang('principal_address') ?></th>
              <th><?= lang('principal_city') ?></th>
              <th><?= lang('principal_pic') ?></th>
            </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>