<div class="col-sm-9 col-md-10 col-sm-push-3 col-md-push-2">
  <h1><?= lang("principal_new") ?></h1>
  <hr>
  <form class="form-horizontal" method="post" action="<?= site_url('principal/save') ?>">
    <div class="form-group">
      <label class="col-md-2 control-label" for="principal_code"><?= lang('principal_code') ?></label>
      <div class="col-md-6">
        <input type="text" id="principal_code" name="principal_code" class="form-control"
               value="<?= isset($principal) ? $principal->code : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="principal_name"><?= lang('principal_name') ?></label>
      <div class="col-md-6">
        <input type="text" id="principal_name" name="principal_name" class="form-control"
               value="<?= isset($principal) ? $principal->name : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="principal_address"><?= lang("principal_address") ?></label>
      <div class="col-md-6">
      <textarea class="form-control" id="principal_address" name="principal_address"
                placeholder="Type in your message"
                rows="3"><?= isset($principal) ? $principal->address : '' ?></textarea>
      <span class="pull-right chars-remaining-pf">
          <?= lang("remaining_chars") ?><span id="charRemainingCntFld"></span>
      </span>
        <script>
          $(function () {
            // countFld is the id of the field where you want the 'remaining chars. count' number
            // to be displayed.
            // $("#pricipal_address").countRemainingChars( {countFld: "charRemainingCntFld"} );

            // all settings/options
            $('#principal_address').countRemainingChars({
              countFld: 'charRemainingCntFld',
              charsMaxLimit: 150,
              charsWarnRemaining: 5,
              blockInputAtMaxLimit: true
            });


            // taId is the id of the textArea field which triggered the event
            // Helpful if counting remaining chars on multiple TAs
            $('#principal_address').on("overCharsMaxLimitEvent", function (event, taId) {
              //$('#postBtn').prop("disabled",true);
            });
            $('#principal_address').on("underCharsMaxLimitEvent", function (event, taId) {
              //$('#postBtn').prop("disabled",false);
            });
          });
        </script>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="principal_city_id"><?= lang("principal_city") ?></label>
      <div class="col-md-6">
        <select class="combobox form-control" id="principal_city_id" name="principal_city_id">
          <option value="" selected="selected"><?= lang('select_city') ?></option>
            <?php foreach ($cities as $city): ?>
              <option value="<?= $city->id ?>" <?php if (isset($principal) && $principal->cityId == $city->id) {echo "selected";} ?>><?= trim($city->name) ?></option>
            <?php endforeach; ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="principal_name"><?= lang('principal_telp') ?></label>
      <div class="col-md-6">
        <input type="text" id="principal_telp" name="principal_telp" class="form-control"
               value="<?= isset($principal) ? $principal->telp : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="principal_name"><?= lang('principal_npwp') ?></label>
      <div class="col-md-6">
        <input type="text" id="principal_npwp" name="principal_npwp" class="form-control"
               value="<?= isset($principal) ? $principal->npwp : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="principal_name"><?= lang('principal_npwp_name') ?></label>
      <div class="col-md-6">
        <input type="text" id="principal_npwp_name" name="principal_npwp_name" class="form-control"
               value="<?= isset($principal) ? $principal->npwpName : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="principal_name"><?= lang('principal_npwp_address') ?></label>
      <div class="col-md-6">
        <input type="text" id="principal_npwp_address" name="principal_npwp_address" class="form-control"
               value="<?= isset($principal) ? $principal->npwpAddress : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="principal_name"><?= lang('principal_pic') ?></label>
      <div class="col-md-6">
        <input type="text" id="principal_pic" name="principal_pic" class="form-control"
               value="<?= isset($principal) ? $principal->pic: '' ?>">
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-10 col-md-offset-2">
        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
        <a href="<?= site_url('principal') ?>" class="btn btn-default"><?= lang('cancel') ?></a>
      </div>
    </div>
  </form>
</div>