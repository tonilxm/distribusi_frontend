<div class="col-sm-9 col-md-10 col-sm-push-3 col-md-push-2">
  <h1><?= lang("outletType_edit") ?></h1>
  <hr>
  <div class="row">
    <div class="col-sm-12">
      <form class="form-horizontal" method="post" action="<?= site_url('outletType/update') ?>">
        <input type="hidden" name="outletType_id" value="<?= $outletType->id ?>">
        <div class="form-group">
          <label class="col-md-2 control-label" for="outletType_code"><?= lang('outletType_code') ?></label>
          <div class="col-md-6">
            <input type="text" id="outletType_code" name="outletType_code" class="form-control"
                   value="<?= $outletType->code ?>" readonly="true">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label" for="outletType_name"><?= lang('outletType_name') ?></label>
          <div class="col-md-6">
            <input type="text" id="outletType_name" name="outletType_name" class="form-control"
                   value="<?= $outletType->name ?>">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-10 col-md-offset-2">
            <button type="submit" class="btn btn-primary update"><?= lang('update') ?></button>
            <button class="btn btn-danger delete" data-toggle="modal" data-target="#confirmDeleteModal" id="btnDelete">
              <?= lang('delete') ?>
            </button>
            <a href="<?= site_url('outletType') ?>" class="btn btn-default"><?= lang('back') ?></a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="confirmDeleteModal" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteModalLabel"
     aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <span class="pficon pficon-close"></span>
        </button>
        <h4 class="modal-title" id="confirmDeleteModalLabel"><?= lang('confirm_delete') ?></h4>
      </div>
      <div class="modal-body">
        <h4 class="center-block"><?= lang('Common.COMM-0002') ?></h4>
      </div>
      <div class="modal-footer">
        <a href="<?= site_url('outletType/'. $outletType->id .'/delete') ?>" class="btn btn-primary" id="btnDelete">
          <?= lang('ok') ?>
        </a>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
      </div>
    </div>
  </div>
</div>