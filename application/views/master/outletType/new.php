<div class="col-sm-9 col-md-10 col-sm-push-3 col-md-push-2">
  <h1><?= lang("outletType_new") ?></h1>
  <hr>
  <form class="form-horizontal" method="post" action="<?= site_url('outletType/save') ?>">
    <div class="form-group">
      <label class="col-md-2 control-label" for="outletType_code"><?= lang('outletType_code') ?></label>
      <div class="col-md-6">
        <input type="text" id="outletType_code" name="outletType_code" class="form-control"
               value="<?= isset($outletType) ? $outletType->code : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="outletType_name"><?= lang('outletType_name') ?></label>
      <div class="col-md-6">
        <input type="text" id="outletType_name" name="outletType_name" class="form-control"
               value="<?= isset($outletType) ? $outletType->name : '' ?>">
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-10 col-md-offset-2">
        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
        <a href="<?= site_url('outletType') ?>" class="btn btn-default"><?= lang('cancel') ?></a>
      </div>
    </div>
  </form>
</div>