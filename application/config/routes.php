<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$lang                                                     = '^(id|en|de|fr|nl)';
$route['default_controller']                              = 'home';
$route['^(id|en|de|fr|nl)$']                              = $route['default_controller'];
$route['404_override']                                    = '';
$route['translate_uri_dashes']                            = FALSE;

$route[$lang.'/login']                                    = 'home/login';
$route[$lang.'/dashboard']                                = 'home/dashboard';

// MASTER PRINCIPAL```
$route[$lang.'/principal']                                = 'master/principal';
$route[$lang.'/principal/new']                            = 'master/principal/new_form';
$route[$lang.'/principal/save']                           = 'master/principal/save';
$route[$lang.'/principal/update']                         = 'master/principal/update';
$route[$lang.'/principal/search']                         = 'master/principal/search';
$route[$lang.'/principal/(:num)/delete']                  = 'master/principal/delete/$2'; // start param at 2 instead of 1
$route[$lang.'/principal/(:num)']                         = 'master/principal/show/$2'; // start param at 2 instead of 1


// MASTER AREA
$route[$lang.'/area']                                = 'master/area';
$route[$lang.'/area/new']                            = 'master/area/new_form';
$route[$lang.'/area/edit']                           = 'master/area/edit_form';
$route[$lang.'/area/save']                           = 'master/area/save';
$route[$lang.'/area/update']                         = 'master/area/update';
$route[$lang.'/area/search']                         = 'master/area/search';
$route[$lang.'/area/(:num)/delete']                  = 'master/area/delete/$2'; // start param at 2 instead of 1
$route[$lang.'/area/(:num)']                         = 'master/area/show/$2'; // start param at 2 instead of 1

// MASTER OUTLET TYPE
$route[$lang.'/outletType']                                = 'master/outletType';
$route[$lang.'/outletType/new']                            = 'master/outletType/new_form';
$route[$lang.'/outletType/edit']                           = 'master/outletType/edit_form';
$route[$lang.'/outletType/save']                           = 'master/outletType/save';
$route[$lang.'/outletType/update']                         = 'master/outletType/update';
$route[$lang.'/outletType/search']                         = 'master/outletType/search';
$route[$lang.'/outletType/(:num)/delete']                  = 'master/outletType/delete/$2'; // start param at 2 instead of 1
$route[$lang.'/outletType/(:num)']                         = 'master/outletType/show/$2'; // start param at 2 instead of 1

// MASTER OUTLET
$route[$lang.'/outlet']                                = 'master/outlet';
$route[$lang.'/outlet/new']                            = 'master/outlet/new_form';
$route[$lang.'/outlet/save']                           = 'master/outlet/save';
$route[$lang.'/outlet/update']                         = 'master/outlet/update';
$route[$lang.'/outlet/search']                         = 'master/outlet/search';
$route[$lang.'/outlet/(:num)/delete']                  = 'master/outlet/delete/$2'; // start param at 2 instead of 1
$route[$lang.'/outlet/(:num)']                         = 'master/outlet/show/$2'; // start param at 2 instead of 1

// MASTER SALESMAN
$route[$lang.'/salesman']                                = 'master/salesman';
$route[$lang.'/salesman/new']                            = 'master/salesman/new_form';
$route[$lang.'/salesman/save']                           = 'master/salesman/save';
$route[$lang.'/salesman/update']                         = 'master/salesman/update';
$route[$lang.'/salesman/search']                         = 'master/salesman/search';
$route[$lang.'/salesman/(:num)/delete']                  = 'master/salesman/delete/$2'; // start param at 2 instead of 1
$route[$lang.'/salesman/(:num)']                         = 'master/salesman/show/$2'; // start param at 2 instead of 1

// MASTER BRAND
$route[$lang.'/brand']                                = 'master/brand';
$route[$lang.'/brand/new']                            = 'master/brand/new_form';
$route[$lang.'/brand/save']                           = 'master/brand/save';
$route[$lang.'/brand/update']                         = 'master/brand/update';
$route[$lang.'/brand/search']                         = 'master/brand/search';
$route[$lang.'/brand/(:num)/delete']                  = 'master/brand/delete/$2'; // start param at 2 instead of 1
$route[$lang.'/brand/(:num)']                         = 'master/brand/show/$2'; // start param at 2 instead of 1

// last rule
$route[$lang.'/(.+)$']                                    = "$2";


