<?php if (!defined('BASEPATH')) {
  exit('No direct script access allowed');
}

/**
 * A Controller base that provides lots of flexible basics for CodeIgniter applications,
 * including:
 *     - A basic template engine
 *     - More readily available 'flash messages'
 *     - autoloading of language and model files
 *     - flexible rendering methods making JSON simple.
 *     - Automatcially migrating.
 *
 * NOTE: This class assumes that a couple of other libraries are in use in your
 * application:
 *     - eldarion-ajax (https://github.com/eldarion/eldarion-ajax) for simple AJAX.
 *         Only used by the render_json method to return profiler info and
 *
 */
class MY_Controller extends CI_Controller
{

  /**
   * The type of caching to use. The default values are
   * set here so they can be used everywhere, but
   */
  protected $cache_type = 'dummy';

  protected $backup_cache = 'file';

  // If TRUE, will send back the notices view
  // through the 'render_json' method in the
  // 'fragments' array.
  protected $ajax_notices = TRUE;

  // If set, this language file will automatically be loaded.
  protected $language_file = NULL;

  // If set, this model file will automatically be loaded.
  protected $model_file = NULL;

  protected $use_view = '';

  protected $use_layout = '';

  protected $scripts = array();

  protected $stylesheets = array();

  // Stores data variables to be sent to the view.
  protected $vars = array();

  // For status messages
  protected $message;

  // Should we try to migrate to the latest version
  // on every page load?
  protected $auto_migrate = FALSE;

  /*
      API STUFF
   */

  // If TRUE, this will initialize the API resources, like Guzzle.
  protected $use_api = TRUE;

  protected $api_version = 'v2';

  // Stores the access token once we've grabbed it.
  protected $api_access_token = NULL;

  // Store the expiration time for the access token.
  protected $api_access_expires = NULL;

  // An instance of the Guzzle library
  protected $guzzle = NULL;

  // Any api error string that might have occurred.
  protected $api_error = NULL;

  protected $lang_code = NULL;

  //--------------------------------------------------------------------

  public function __construct()
  {
    parent::__construct();

    //--------------------------------------------------------------------
    // Timezone Setup
    //--------------------------------------------------------------------
    $timezone = $this->config->item('date.timezone');
    date_default_timezone_set($timezone);

    //--------------------------------------------------------------------
    // I18N Setup (Languages)
    //--------------------------------------------------------------------
    $uri = uri_string();
    $this->lang_code = $this->get_uri_lang($uri);

    $this->lang->load('label_lang');
    $this->lang->load('message_lang');


    //--------------------------------------------------------------------
    // Cache Setup
    //--------------------------------------------------------------------

    // Make sure that caching is ALWAYS available throughout the app
    // though it defaults to 'dummy' which won't actually cache.
    $this->load->driver('cache', array(
      'adapter' => $this->cache_type,
      'backup' => $this->backup_cache
    ));

    //--------------------------------------------------------------------
    // Language & Model Files
    //--------------------------------------------------------------------
    if (!is_null($this->language_file)) {
      $this->lang->load($this->language_file);
    }

    if (!is_null($this->model_file)) {
      /*
          This does not automatically load the database.
          If you're using my MY_Model, it will load it for you.
          otherwise, you should load it in your autoload config
          or within your model itself.
       */
      $this->load->model($this->model_file);
    }

    //--------------------------------------------------------------------
    // Profiler
    //--------------------------------------------------------------------

    // The profiler is dealt with twice so that we can set
    // things up to work correctly in AJAX methods using $this->render_json
    // and it's cousins.
    if ($this->config->item('show_profiler') == TRUE) {
      $this->load->add_package_path(APPPATH . 'third_party/forensics/');
      $this->output->enable_profiler(TRUE);
    }

    //--------------------------------------------------------------------
    // Development Environment Setup
    //--------------------------------------------------------------------
    //
    if (ENVIRONMENT == 'development') {
    }

    //--------------------------------------------------------------------

    // $this->stylesheets[] = '/assets/css/app.css';

    //--------------------------------------------------------------------
    // Load API Resources
    //--------------------------------------------------------------------

    $api_url = config_item('api.baseURL');

    $this->guzzle = new \Guzzle\Http\Client($api_url);
    $this->guzzle->setSslVerification(FALSE);

    // Check the session for an access_token and other access info...
    $this->api_access_token = $this->session->token;
    $this->api_access_expires = $this->session->tokenExpires;


  }

  //check if the language exists
  //when true returns an array with lang abbreviation + rest
  public function get_uri_lang($uri = '')
  {
    if (!empty($uri)) {
      $uri = ($uri[0] == '/') ? substr($uri, 1) : $uri;

      $uri_expl = explode('/', $uri, 2);

      return $uri_expl[0];
    } else
      return FALSE;
  }

  //--------------------------------------------------------------------

  //--------------------------------------------------------------------
  // "Template" Functions
  //--------------------------------------------------------------------

  /**
   * A Very simple templating system designed not for power or flexibility
   * but to use the built in features of CodeIgniter's view system to easily
   * create fast templating capabilities.
   *
   * The view is assumed to be under the views folder, under a folder with the
   * name of the controller and a view matching the name of the method.
   *
   * The theme is simply a set of files located under the views/ui folder. By default
   * a view named index.php will be used. You can specify different layouts
   * with the scope method, 'layout()'.
   *
   *      $this->layout('two_left')->render();
   *
   * You can specify a non-default view name with the scope method 'view'.
   *
   *      $this->view('another_view')->render();
   *
   * Within the template the string '{view_content}' will be replaced with the
   * contents of the view file that we're rendering.
   *
   * @param  [type]  $layout      [description]
   * @param  boolean $return_data [description]
   * @return [type]               [description]
   */
  protected function render($data = array())
  {
    // Calc our view name based on current method/controller
    $dir = $this->router->fetch_directory();
    if (strpos($dir, 'modules')) {
      $dir = str_replace('../modules/', '', $dir);
      $dir = str_replace('controllers/', '', $dir);
    }

    if ($dir == $this->router->fetch_module() . '/') {
      $dir = '';
    }

    $view = !empty($this->use_view) ? $this->use_view :
      $dir . $this->router->fetch_class() . '/' . $this->router->fetch_method();

    // Merge any saved vars into the data
    $data = array_merge($data, $this->vars);

    // Make sure any scripts/stylesheets are available to the view
    $data['scripts'] = $this->scripts;
    $data['stylesheets'] = $this->stylesheets;

    // Build our notices from the theme's view file.
    $data['notice'] = $this->message();

    if (isset($this->current_user) && !empty($this->current_user)) {
      $data['current_user'] = $this->current_user;
    }

    // We'll make the view content available to the template.
    $data['view_content'] = $this->load->view($view, $data, TRUE);

    // Render our layout and we're done!
    $layout = !empty($this->use_layout) ? $this->use_layout : 'index';

    $this->load->view('theme/' . $layout, $data, FALSE, TRUE);

    // Reset our custom view attributes.
    $this->use_view = $this->use_layout = '';
  }

  //--------------------------------------------------------------------

  /**
   * Sets a data variable to be sent to the view during the render() method.
   *
   * @param string $name
   * @param mixed $value
   */
  public function set_var($name, $value = NULL)
  {
    if (is_array($name)) {
      foreach ($name as $k => $v) {
        $this->vars[$k] = $v;
      }
    } else {
      $this->vars[$name] = $value;
    }
  }

  //--------------------------------------------------------------------

  /**
   * Specifies a custom view file to be used during the render() method.
   * Intended to be used as a chainable 'scope' method prioer to calling
   * the render method.
   *
   * Examples:
   *      $this->view('my_view')->render();
   *      $this->view('users/login')->render();
   *
   * @param  string $view The relative path/name of the view file to use.
   * @return MY_Controller instance
   */
  public function view($view)
  {
    $this->use_view = $view;

    return $this;
  }

  //--------------------------------------------------------------------

  /**
   * Specifies a custom layout file to be used during the render() method.
   * Intended to be used as a chainable 'scope' method prioer to calling
   * the render method.
   *
   * Examples:
   *      $this->layout('two_left')->render();
   *
   * @param  string $view The relative path/name of the view file to use.
   * @return MY_Controller instance
   */
  public function layout($view)
  {
    $this->use_layout = $view;

    return $this;
  }

  //--------------------------------------------------------------------

  //--------------------------------------------------------------------
  // Status Messages
  //--------------------------------------------------------------------

  /**
   * Sets a status message (for displaying small success/error messages).
   * This is used in place of the session->flashdata functions since you
   * don't always want to have to refresh the page to show the message.
   *
   * @param string $message The message to save.
   * @param string $type The string to be included as the CSS class of the containing div.
   */
  public function set_message($message = '', $type = 'info')
  {
    if (!empty($message)) {
      if (isset($this->session)) {
        $this->session->set_flashdata('message', $type . '::' . $message);
      }

      $this->message = array(
        'type' => $type,
        'message' => $message
      );
    }
  }

  //--------------------------------------------------------------------

  /**
   * Retrieves the status message to display (if any).
   *
   * @param  string $message [description]
   * @param  string $type [description]
   * @return array
   */
  public function message($message = '', $type = 'info')
  {
    $return = array(
      'message' => $message,
      'type' => $type
    );

    // Does session data exist?
    if (empty($message) && class_exists('CI_Session')) {
      $message = $this->session->flashdata('message');

      if (!empty($message)) {
        // Split out our message parts
        $temp_message = explode('::', $message);
        $return['type'] = $temp_message[0];
        $return['message'] = $temp_message[1];

        unset($temp_message);
      }
    }

    // If message is empty, we need to check our own storage.
    if (empty($message)) {
      if (empty($this->message['message'])) {
        return '';
      }

      $return = $this->message;
    }

    // Clear our session data so we don't get extra messages on rare occassions.
    if (class_exists('CI_Session')) {
      $this->session->set_flashdata('message', '');
    }

    return $return;
  }

  //--------------------------------------------------------------------

  //--------------------------------------------------------------------
  // Other Rendering Methods
  //--------------------------------------------------------------------

  /**
   * Renders a string of aribritrary text. This is best used during an AJAX
   * call or web service request that are expecting something other then
   * proper HTML.
   *
   * @param  string $text The text to render.
   * @param  bool $typography If TRUE, will run the text through 'Auto_typography'
   *                            before outputting to the browser.
   *
   * @return [type]       [description]
   */
  public function render_text($text, $typography = FALSE)
  {
    // Note that, for now anyway, we don't do any cleaning of the text
    // and leave that up to the client to take care of.

    // However, we can auto_typogrify the text if we're asked nicely.
    if ($typography === TRUE) {
      $this->load->helper('typography');
      $text = auto_typography($text);
    }

    $this->output->enable_profiler(FALSE)
      ->set_content_type('text/plain')
      ->set_output($text);
  }

  //--------------------------------------------------------------------

  /**
   * Converts the provided array or object to JSON, sets the proper MIME type,
   * and outputs the data.
   *
   * Do NOT do any further actions after calling this action.
   *
   * @param  mixed $json The data to be converted to JSON.
   * @return [type]       [description]
   */
  public function render_json($json)
  {
    if (is_resource($json)) {
      throw new RenderException('Resources can not be converted to JSON data.');
    }

    // If there is a fragments array and we've enabled profiling,
    // then we need to add the profile results to the fragments
    // array so it will be updated on the site, since we disable
    // all profiling below to keep the results clean.
    if (is_array($json)) {
      if (!isset($json['fragments'])) {
        $json['fragments'] = array();
      }

      if ($this->config->item('show_profile')) {
        $this->load->library('profiler');
        $json['fragments']['#profiler'] = $this->profiler->run();
      }

      // Also, include our notices in the fragments array.
      if ($this->ajax_notices === TRUE) {
        $json['fragments']['#notices'] = $this->load->view('theme/notice', array('notice' => $this->message()), TRUE);
      }
    }

    $this->output->enable_profiler(FALSE)
      ->set_content_type('application/json')
      ->set_output(json_encode($json));
  }

  //--------------------------------------------------------------------

  /**
   * Sends the supplied string to the browser with a MIME type of text/javascript.
   *
   * Do NOT do any further processing after this command or you may receive a
   * Headers already sent error.
   *
   * @param  mixed $js The javascript to output.
   * @return [type]       [description]
   */
  public function render_js($js = NULL)
  {
    if (!is_string($js)) {
      throw new RenderException('No javascript passed to the render_js() method.');
    }

    $this->output->enable_profiler(FALSE)
      ->set_content_type('application/x-javascript')
      ->set_output($js);
  }

  //--------------------------------------------------------------------

  /**
   * Breaks us out of any output buffering so that any content echo'd out
   * will echo out as it happens, instead of waiting for the end of all
   * content to echo out. This is especially handy for long running
   * scripts like might be involved in cron scripts.
   *
   * @return void
   */
  public function render_realtime()
  {
    if (ob_get_level() > 0) {
      end_end_flush();
    }
    ob_implicit_flush(TRUE);
  }

  //--------------------------------------------------------------------

  /**
   * Integrates with the bootstrap-ajax javascript file to
   * redirect the user to a new url.
   *
   * If the URL is a relative URL, it will be converted to a full URL for this site
   * using site_url().
   *
   * @param  string $location [description]
   */
  public function ajax_redirect($location = '')
  {
    $location = empty($location) ? '/' : $location;

    if (strpos($location, '/') !== 0 || strpos($location, '://') !== FALSE) {
      if (!function_exists('site_url')) {
        $this->load->helper('url');
      }

      $location = site_url($location);
    }

    $this->render_json(array('location' => $location));
  }

  //--------------------------------------------------------------------

  /**
   * Attempts to get any information from php://input and return it
   * as JSON data. This is useful when your javascript is sending JSON data
   * to the application.
   *
   * @param  strign $format The type of element to return, either 'object' or 'array'
   * @param  int $depth The number of levels deep to decode
   *
   * @return mixed    The formatted JSON data, or NULL.
   */
  public function get_json($format = 'object', $depth = 512)
  {
    $as_array = $format == 'array' ? TRUE : FALSE;

    return json_decode(file_get_contents('php://input'), $as_array, $depth);
  }

  //--------------------------------------------------------------------

  //--------------------------------------------------------------------
  // 'Asset' functions
  //--------------------------------------------------------------------

  /**
   * Adds an javascript file to the 'script' array.
   *
   * @param [type] $filename [description]
   */
  public function add_script($filename)
  {
    if (strpos($filename, 'http') === FALSE) {
      $filename = base_url() . 'assets/app/js/pages/' . $filename;
    }

    $this->scripts[] = $filename;
  }

  //--------------------------------------------------------------------

  /**
   * Adds an external stylesheet file to the 'stylesheets' array.
   */
  public function add_style($filename)
  {
    if (strpos($filename, 'http') === FALSE) {
      $filename = base_url() . 'assets/css/' . $filename;
    }

    $this->stylesheets[] = $filename;
  }

  //--------------------------------------------------------------------

  //--------------------------------------------------------------------
  // API Methods
  //--------------------------------------------------------------------

  /**
   * Make a request to the API.
   *
   * @param      $method 'get', 'post', 'delete', etc.
   * @param      $url
   * @param null $data
   *
   * @return array $response
   */
  public function request($method, $url, $data = NULL, $skip_expiration_token_check = false)
  {
    $request = NULL;
    $header = [
      'Authorization' => 'Bearer ' . $this->api_access_token
    ];

    /*
     * Token Refresh Mechanism Based on this article:
     * 1. http://stackoverflow.com/questions/26739167/jwt-json-web-token-automatic-prolongation-of-expiration
     *
     * Check if current token is valid (server side handles this)
     * and assign new token if token expires within 5 minutes
     *
     */
    if ($skip_expiration_token_check == false) {
      if ($this->api_access_token && $this->session->tokenExpires) {
        $current_expiredate = DateTime::createFromFormat('d/m/Y H:i:s', $this->session->tokenExpires);
        $now = new DateTime("now");

        $diff = $now->diff($current_expiredate, true);
        $diff_in_minutes = ($diff->h * 60) + $diff->i;

        if ($diff_in_minutes <= 5) {
          $request = $this->guzzle->post('oauth/refreshToken', $header,
            Array('expire_period' => $this->config->item('sess_expiration')));

          $response = $request->send();

          $response = (array)json_decode($response->getBody(TRUE));

          $this->session->token = $this->api_access_token = $response['token'];

          $this->session->tokenExpires = $this->api_access_expires = $response['tokenExpires'];
        }
      }
    }

    switch ($method) {
      case 'post':
        $request = $this->guzzle->post($url, $header, $data);
        break;
      case 'put':
        if (is_null($data)) {
          $data = [];
        }
        $header['Content-Type'] = 'application/x-www-form-urlencoded';
        $request = $this->guzzle->put($url, $header, http_build_query($data));
        break;
      case 'delete':
        $request = $this->guzzle->delete($url, $header);
        break;
      case 'head':
        $request = $this->guzzle->head($url, $header);
        break;
      case 'get':
      default:
        $request = $this->guzzle->get($url, $header);
        break;
    }

    // Set our authorization header -> this doesnt works!
    /*
    if (! empty($this->api_access_token))
    {
        $request->setHeader('Authorization', 'Bearer ' . $this->api_access_token);
    }
    */

    try {
      $response = $request->send();
      $status = $response->getStatusCode();

      $response = (array)json_decode($response->getBody(TRUE));
      $response['statusCode'] = $status;
    } catch (Exception $e) {
      $response = (array)json_decode($e->getResponse()
        ->getBody(TRUE));
      $response['serverMessage'] = $e->getMessage();
      $response['statusCode'] = $e->getResponse()
        ->getStatusCode();
    }

    if ($response['statusCode'] == 401) {
      redirect("/");
    }

    return $response;
  }
  //--------------------------------------------------------------------

}

//--------------------------------------------------------------------

/*
    AUTHENTICATED CONTROLLER

    Simply makes sure that someone is logged in and ready to roll.
 */

class Authenticated_Controller extends MY_Controller
{

  protected $restrict_to = NULL;

  protected $strict_restrict = FALSE;

  /*
      CURRENT USER STUFF
   */
  // The current, logged in, user id
  protected $current_user;

  //--------------------------------------------------------------------

  public function __construct()
  {
    parent::__construct();

    $this->set_current_user();

    // TODO: Apply Roles!
    // Check our authentication restrictions here.
    // $this->restrict($this->restrict_to, $this->strict_restrict);
  }

  //--------------------------------------------------------------------

  /**
   * Checks the session to see if the user has been logged in or not.
   *
   * @return bool
   */
  public function is_logged_in()
  {
    return (bool)$this->session->api_access_token;
  }

  //--------------------------------------------------------------------

  /**
   * Ensures that we are storing our current user information
   * from the session into the class for easy use anywhere.
   */
  public function set_current_user()
  {
    $this->current_user = $this->session->user;
  }
  //--------------------------------------------------------------------

  /**
   * Returns either a single piece of information about the current user
   * or the entire user array.
   *
   * @param null $item
   * @return bool|mixed
   */
  public function current_user($item = NULL)
  {
    if (!empty($item)) {
      if (isset($this->current_user->$item)) {
        return $this->current_user->$item;
      }

      return FALSE;
    }

    return $this->current_user;
  }

  //--------------------------------------------------------------------

  /**
   * Ensures that the user is logged in. Optionally, can specify a specific
   * role to restrict to, and whether it must be exactly that role or any
   * role with that permission or higher.
   *
   * @param string $restrict_to The name of the role to restrict to
   * @param bool $strict If TRUE, roles must match exactly
   * @return bool
   */
  public function restrict($restrict_to = NULL, $strict = NULL)
  {
    $this->load->library('roles');

    $restrict_to = empty($restrict_to) ? $this->restrict_to : $restrict_to;
    $strict = is_null($strict) ? $this->strict_restrict : FALSE;

    $role = $this->roles->find_id_from_name(isset($this->current_user->role) ? $this->current_user->role : NULL);

    // If no restrict_to is set, just verify that we're logged in...
    if (empty($restrict_to)) {
      if (!$this->is_logged_in()) {
        $this->set_message('You must be logged in to view this page', 'error');
        redirect('login');
      }

      return;
    }

    if (!$answer = $this->roles->restrict($role, $restrict_to, $strict)) {
      if ($this->is_logged_in()) {
        $this->set_message('You do not have sufficient permissions to view this page.', 'warning');
        redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/login');
      }

      $this->set_message('You must be logged in to view this page', 'warning');
      redirect('login');
    }

    // Is our API token still valid?
    $expires = $this->session->userdata('api_expires');

    if ($expires < time()) {
      $this->set_message('You must be logged in to renew your API token.', 'warning');
      redirect('login');
    }

    return TRUE;
  }
  //--------------------------------------------------------------------


}

//--------------------------------------------------------------------

class Admin_Controller extends Authenticated_Controller
{

  protected $restrict_to = 'superadmin';

  protected $use_layout = 'admin';
}