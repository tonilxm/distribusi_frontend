<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller
{

  protected $model_file = 'users/user_model';

  public function __construct()
  {
    parent::__construct();

  }

  //--------------------------------------------------------------------


  public function index()
  {
    redirect('login');
  }


  public function login()
  {
    $data = [];

    if ($this->input->post('login')) {
      $data = [
        'user_name' => $this->input->post('username'),
        'password' => $this->input->post('password'),
        'expire_period' => $this->config->item('sess_expiration')
      ];

      // Validate with the API and get our client_info...
      $query = $this->request('post', 'oauth/login', $data, true);

      if ($query['statusCode'] != 200) {
        $this->set_message('Invalid username and password!', 'info');
        $this->set_var($data);

        $this->render();
        return;
      } else {
        $this->api_access_token = $this->session->token = $query['tokenInfo']->token;
        $this->api_access_expires = $this->session->tokenExpires = $query['tokenInfo']->tokenExpires;
        $this->session->user = $query['user']->id;

        redirect("principal");
      }
    }

    if ($error = $this->session->flashdata('error'))
      $this->set_var('error', $error);

    // this just sample for displaying notice
    $message = "These examples are included for development testing purposes.  " .
      "For official documentation, see <a href=\"https://www.patternfly.org\" " .
      "class=\"alert-link\">https://www.patternfly.org</a> and <a href=\"http://getbootstrap.com\" " .
      "class=\"alert-link\">http://getbootstrap.com</a>.";

    $this->set_message($message, 'warning');
    $this->set_var($data);
    $this->render();
  }

  public function test()
  {
    $query = $this->request("get", "oauth/test");

    $this->render();
  }

  //--------------------------------------------------------------------

  public function logout()
  {
    $this->session->sess_destroy();
    redirect('/');
  }

  //--------------------------------------------------------------------

  public function forgot_password()
  {
    $this->load->library('form_validation');

    $errors = array();

    if ($this->input->post('submit')) {
      $email = $this->input->post('email', TRUE);

      // Empty email?
      if (empty($email)) {
        $errors[] = 'The email address cannot be blank.';
      } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors[] = 'You must provide a valid email address.';
      } else if (!$user = $this->user_model->find_by('email', $email)) {
        $errors[] = 'Unable to find an account with that email address.';
      }

      if (!count($errors)) {
        if ($this->send_forgotten_email($user)) {
          $this->set_var('email_sent', true);
        } else {
          $this->set_var('email_sent', false);
        }
      }

      $this->set_var('errors', implode('<br/>', $errors));
    }

    $this->render();
  }

  //--------------------------------------------------------------------

  /**
   * Verifies the users' code and allows them to choose
   * a new password.
   *
   * @param  string $email
   * @param  string $code
   *
   * @return mixed
   */
  public function reset_password($email = null, $code = null)
  {
    $this->load->library('form_validation');

    $email = empty($email) ? null : base64_decode($email);

    if ($this->input->post('submit')) {
      // Grab our user so that we can verify, once again,
      // that the user and the code match up.
      $email = trim($this->input->post('email', TRUE));
      $code = trim($this->input->post('code', TRUE));

      if (!$user = $this->user_model->find_by(array('email' => $email, 'reset_hash' => $code))) {
        die('Unable to save new password. Possible hacking attempt.');
      }

      $this->user_model->require_field('password');
      $this->user_model->require_field('pass_confirm');

      $data = array(
        'password' => $this->input->post('password'),
        'pass_confirm' => $this->input->post('pass_confirm')
      );

      if ($this->user_model->update($user->id, $data)) {
        $this->set_message('Your password was successfully reset. Please login below.', 'success');
        return redirect('login');
      }
    }

    // If we don't have a reset code, we can't
    // allow them to reset since we don't know for who
    // and if they should be allowed.
    $can_reset = false;

    if (!is_null($code) && $user = $this->user_model->find_by('reset_hash', $code)) {
      $can_reset = true;
    } else {
      $email = null;
    }

    $view_data = array(
      'can_reset' => $can_reset,
      'code' => $code,
      'email' => $email,
      'site_name' => config_item('site_name'),
    );
    $this->set_var($view_data);

    $this->render();
  }

  //--------------------------------------------------------------------

  /**
   * Sends the email to the user with the link back here to reset their
   * password.
   *
   * @param  object $user The user object
   *
   * @return void
   */
  private function send_forgotten_email($user)
  {
    $this->load->helper('string');
    $code = random_string('unique');

    /*
        Save to database
     */
    $data = array(
      'reset_hash' => $code
    );

    if (!$this->user_model->update($user->id, $data)) {
      if (!validation_errors()) {
        $this->set_message('Unable to send code. ' . $this->user_model->get_db_error_message('dbw'), 'alert');
      }
      return FALSE;
    }

    /*
        Send the email
     */
    $this->load->library('email');

    $this->email->to($user->email);
    $this->email->from($this->config->item('site_email'));
    $this->email->subject('Password Reset Instructions');

    $email_data = array(
      'site_name' => $this->config->item('site_name'),
      'site_email' => $this->config->item('site_email'),
      'users_name' => $this->input->post('name'),
      'users_email' => $this->input->post('email'),
      'reset_link' => site_url('/reset_password'),
      'code' => $code
    );

    $this->email->message($this->load->view('emails/reset_password', $email_data, true));

    if ($this->email->send()) {
      if (ENVIRONMENT == 'development') {
//                echo '<pre>' .$this->email->print_debugger() .'</pre>';
      }

      return TRUE;
    }

    if (ENVIRONMENT == 'development') {
      die($this->email->print_debugger());
    }

    $this->set_message('There was an error sending the email. Please contact the system admin to get your account verified manually.', 'error');

    return FALSE;
  }

  //--------------------------------------------------------------------

  public function imagefix()
  {
    $total = $this->db->like('answer', '/Have')->from('responses')->count_all_results();

    $query = $this->db->like('answer', '/Have')->get('responses');

    if (!$query->num_rows()) {
      die('No records found.');
    }

    $rows = $query->result();

    foreach ($rows as $row) {
      if (empty ($row->answer) || strpos($row->answer, 'haven/Q') !== 0) {
        continue;
      }

      $new_name = str_replace('/Have', '/HAVE', $row->answer);

      $this->db->where('id', (int)$row->id)
        ->set('answer', $new_name)
        ->update('responses');

      echo 'Done: ' . $row->id . "\n";
    }

    //--------------------------------------------------------------------

  }

  public function dashboard()
  {
    $this->render();
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */