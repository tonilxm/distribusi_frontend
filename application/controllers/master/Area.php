<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Area extends Authenticated_Controller
{
  public function __construct()
  {
    parent::__construct();

    $this->layout('master');
  }

  /**
   *
   */
  public function index() {
    $this->add_script("area-index");
    $this->view('master/area/search')->render();
  }

  /**
   *
   */
  public function new_form()
  {
    $this->view("master/area/new")->render();
  }

  /**
   *
   */
  public function save()
  {

    $data = $this->_getPostData();
    $response = $this->request('put', 'area/save', $data);
    if ($response['error']) {
      $data['area'] = $response['data'];
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      $this->view("master/area/new")->render($data);
      return;
    }

    $message = $this->lang->line('Area.AREA-0001');
    $this->set_message($message, 'info');

    redirect("area");
  }

  /**
   *
   */
  public function update()
  {
    $data = $this->_getPostData();

    $response = $this->request('post', 'area/update', $data);

    if ($response['error']) {
      $data['area'] = $response['data'];
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      $this->view("master/area/show")->render($data);
      return;
    }

    $message = $this->lang->line('Area.AREA-0002');
    $this->set_message($message, 'info');

    redirect("area");
  }

  /**
   * @param $id
   */
  public function delete($id)
  {
    $response = $this->request('delete', 'area/' . $id . '/delete');

    if ($response['error']) {
      $message = $this->lang->line('Common.COMM-0004');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      redirect("brand/". $id);
    }

    $message = $this->lang->line('Area.AREA-0003');
    $this->set_message($message, 'info');

    redirect("area");
  }

  /**
   * Ajax search function called by Datatable
   * @return json data
   */
  public function search()
  {
    // default sort by col-idx-1
    $order_col_idx = $this->input->get("order[0][column]")?$this->input->get("order[0][column]"):1;
    $draw = $this->input->get("draw")?$this->input->get("draw"):1;

    // set order column name based on column idx
    if ((int)$order_col_idx == 0 ) {
      $order_col_name = "code";
    } elseif ((int)$order_col_idx == 1) {
      $order_col_name = "name";
    }

    $order_col_dir = $this->input->get("order[0][dir]")?$this->input->get("order[0][dir]"):"asc";
    $offset = $this->input->get("start");
    $searchCodeValue = $this->input->get("columns[0][search][value]");
    $searchNameValue = $this->input->get("columns[1][search][value]");

    $data = [
      "area_code"           =>  $searchCodeValue,
      "area_name"           =>  $searchNameValue,
      "area_status"         => 'A',
      "offset"              =>  $offset,
      "limit"               =>  $this->input->get("length"),
      "order_column"        =>  $order_col_name,
      "order_dir"           =>  $order_col_dir,
      "draw"                => $draw
    ];

    $response = $this->request("get", "area/search?" . http_build_query($data));

    $dt_rows = [];

    if ($response['statusCode'] == 200) {
      $dt_rows = [
        "draw"              => $draw + 1,
        "recordsTotal"      => (int) $response['recordsTotal'],
        "recordsFiltered"   => (int) $response['recordsFiltered'],
        "data"              => $response['data']
      ];
    }

    echo json_encode($dt_rows);
  }

  /**
   * @param $id
   */
  public function show($id)
  {
    $data = [];

    $response_area = $this->request('get', 'area/' . $id);

    $data['area'] = $response_area['data'];

    $this->add_script("area-show");

    $this->render($data);
  }

  /**
   * @return array
   */
  private function _getPostData() {
    $id = $this->input->post("area_id");
    $name = $this->input->post("area_name");
    $code = $this->input->post("area_code");

    $data = [
      "area_id"       => $id,
      "area_name"     => $name,
      "area_code"     => $code
    ];

    return $data;
  }
}
