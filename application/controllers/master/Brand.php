<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Brand extends Authenticated_Controller
{
  public function __construct()
  {
    parent::__construct();

    $this->layout('master');
  }

  public function index() {
    $this->add_script("brand-search");
    $this->view('master/brand/search')->render();
  }

  /**
   *
   */
  public function new_form()
  {
    $data = [];

    $response = $this->request('get', 'principal/list_all?orderby=name&ordertype=asc');
    $data['principals'] = $response['data'];

    $this->add_script("brand-new");
    $this->view("master/brand/new")->render($data);
  }

  public function save()
  {

    $data = $this->_getPostData();

    $response = $this->request('put', 'brand/save', $data);

    if ($response['error']) {
      $data['brand'] = $response['data'];
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      $this->view("master/brand/new")->render($data);
      return;
    }

    $message = $this->lang->line('Brand.BRND-0001');
    $this->set_message($message, 'info');
    
    redirect("brand");
  }

  public function update()
  {
    $data = $this->_getPostData();
    $response = $this->request('post', 'brand/update', $data);

    if ($response['error']) {
      $data['brand'] = $response['data'];
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      $this->view("master/brand/show")->render($data);
      return;
    }

    $message = $this->lang->line('Brand.BRND-0002');
    $this->set_message($message, 'info');

    redirect("brand");
  }

  public function delete($id)
  {
    $response = $this->request('delete', 'brand/' . $id . '/delete');

    if ($response['error']) {
      $message = $this->lang->line('Common.COMM-0004');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      redirect("brand/". $id);
    }

    $message = $this->lang->line('Brand.BRND-0003');
    $this->set_message($message, 'info');

    redirect("brand");
  }

  /**
   * Ajax search function called by Datatable
   * @return json data
   */
  public function search()
  {
    // default sort by col-idx-1
    $order_col_idx = $this->input->get("order[0][column]");
    $draw = $this->input->get("draw")?$this->input->get("draw"):1;

    // set order column name based on column idx
    if ((int)$order_col_idx == 0 ) {
      $order_col_name = "brand.code";
    } elseif ((int)$order_col_idx == 1) {
      $order_col_name = "brand.name";
    } elseif ((int)$order_col_idx == 2) {
      $order_col_name = "principal.name";
    }

    $order_col_dir = $this->input->get("order[0][dir]")?$this->input->get("order[0][dir]"):"asc";
    $offset = $this->input->get("start");
    $searchCodeValue = $this->input->get("columns[0][search][value]");
    $searchNameValue = $this->input->get("columns[1][search][value]");

    $data = [
      "brand_code"              =>  $searchCodeValue,
      "brand_name"              =>  $searchNameValue,
      "brand_status"            =>  'A',
      "offset"                    =>  $offset,
      "limit"                     =>  $this->input->get("length"),
      "order_column"              =>  $order_col_name,
      "order_dir"                 =>  $order_col_dir,
      "draw"                      => $draw
    ];

    $response = $this->request("get", "brand/search?" . http_build_query($data));

    $dt_rows = [];

    if ($response['statusCode'] == 200) {
      $dt_rows = [
        "draw"              => $draw + 1,
        "recordsTotal"      => (int) $response['recordsTotal'],
        "recordsFiltered"   => (int) $response['recordsFiltered'],
        "data"              => $response['data']
      ];
    }

    echo json_encode($dt_rows);
  }

  public function show($id)
  {
    $data = [];
    $response_brand = $this->request('get', 'brand/' . $id);
    $data['brand'] = $response_brand['data'];

    $response_principals = $this->request('get', 'principal/list_all?orderby=name&ordertype=asc');
    $data['principals'] = $response_principals['data'];

    $this->add_script("brand-show");
    $this->render($data);
  }

  private function _getPostData() {
    $id         = $this->input->post("brand_id");
    $name       = $this->input->post("brand_name");
    $code       = $this->input->post("brand_code");
    $principal  = $this->input->post("brand_principal_id");

    $data = [
      "brand_id"                => $id,
      "brand_name"              => $name,
      "brand_code"              => $code,
      "brand_principal_id"      => $principal,
      "brand_status"            => "A"
    ];

    return $data;
  }
}
