<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Principal extends Authenticated_Controller
{
  public function __construct()
  {
    parent::__construct();

    $this->layout('master');
  }

  public function index() {
    $this->add_script("principal-index");
    $this->view('master/principal/search')->render();
  }

  /**
   *
   */
  public function new_form()
  {
    $data = [];
    $response = $this->request('get', 'city/get_all?orderby=type,name&ordertype=desc,asc');
    $data['cities'] = $response['data'];

    $this->add_script("principal-new");
    $this->view("master/principal/new")->render($data);
  }

  public function save()
  {
    $data = $this->_getPostData();

    $response = $this->request('put', 'principal/save', $data);
    if ($response['error']) {
      $data['principal'] = $response['data'];
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      $response_city = $this->request('get', 'city/get_all?orderby=type,name&ordertype=desc,asc');
      $data['cities'] = $response_city['data'];

      $this->add_script("principal-new");
      $this->view("master/principal/new")->render($data);
      return;
    }

    $message = $this->lang->line('Principal.PRCP-0001');
    $this->set_message($message, 'info');

    redirect("principal");
  }

  public function update()
  {
    $data = $this->_getPostData();

    $response = $this->request('post', 'principal/update', $data);
    if ($response['error']) {
      $data['principal'] = $response['data'];
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      $this->view("master/principal/show")->render($data);
      return;
    }

    $message = $this->lang->line('Principal.PRCP-0002');
    $this->set_message($message, 'info');

    redirect("principal");
  }

  public function delete($id)
  {
    $response = $this->request('delete', 'principal/' . $id . '/delete');
    if ($response['error']) {
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      redirect("principal/" . $id);
    }

    $message = $this->lang->line('Principal.PRCP-0003');
    $this->set_message($message, 'info');

    redirect("principal");
  }

  /**
   * Ajax search function called by Datatable
   * @return json data
   */
  public function search()
  {
    $order_col_idx = $this->input->get("order[0][column]")?$this->input->get("order[0][column]"):0;
    $order_col_name = "name"; // default column name (col-0)
    $draw = $this->input->get("draw")?$this->input->get("draw"):1;

    // set order column name based on column idx
    if ((int)$order_col_idx == 1 ) {
      $order_col_name = "address";
    } elseif ((int)$order_col_idx == 2) {
      $order_col_name = "city";
    }

    $order_col_dir = $this->input->get("order[0][dir]")?$this->input->get("order[0][dir]"):"asc";
    $offset = $this->input->get("start");
    $searchNameValue = $this->input->get("columns[0][search][value]");
    $searchAddressValue = $this->input->get("columns[1][search][value]");

    $data = [
      "principal_name"      =>  $searchNameValue,
      "principal_address"   =>  $searchAddressValue,
      "principal_status"    =>  "A",
      "offset"              =>  $offset,
      "limit"               =>  $this->input->get("length"),
      "order_column"        =>  $order_col_name,
      "order_dir"           =>  $order_col_dir,
      "draw"                => $draw
    ];

    $response = $this->request("get", "principal/search?" . http_build_query($data));

    $dt_rows = [];

    if ($response['statusCode'] == 200) {

      $dt_rows = [
        "draw"              => $draw + 1,
        "recordsTotal"      => (int) $response['recordsTotal'],
        "recordsFiltered"   => (int) $response['recordsFiltered'],
        "data"              => $response['data']
      ];
    }

    echo json_encode($dt_rows);
  }

  public function list_all()
  {

  }

  public function show($id)
  {
    $data = [];

    $response_principal = $this->request('get', 'principal/' . $id);

    $data['principal'] = $response_principal['data'];

    $response_city = $this->request('get', 'city/get_all?orderby=type,name&ordertype=desc,asc');

    $data['cities'] = $response_city['data'];

    $this->add_script("principal-show");
    $this->render($data);
  }

  private function _getPostData() {
    $id = $this->input->post("principal_id");
    $code = $this->input->post("principal_code");
    $name = $this->input->post("principal_name");
    $address = $this->input->post("principal_address");
    $city_id = $this->input->post("principal_city_id");
    $telp = $this->input->post("principal_telp");
    $npwp = $this->input->post("principal_npwp");
    $npwp_name = $this->input->post("principal_npwp_name");
    $npwp_address = $this->input->post("principal_npwp_address");
    $pic = $this->input->post("principal_pic");

    $data = [
      "principal_id"      => $id,
      "principal_code"    => $code,
      "principal_name"    => $name,
      "principal_address" => $address,
      "principal_city_id" => $city_id,
      "principal_telp"    => $telp,
      "principal_npwp"    => $npwp,
      "principal_npwp_name" => $npwp_name,
      "principal_npwp_address" => $npwp_address,
      "principal_pic" => $pic,
      "principal_status"  => "A"
    ];

    return $data;
  }
}
