<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Salesman extends Authenticated_Controller
{
  public function __construct()
  {
    parent::__construct();

    $this->layout('master');
  }

  public function index() {
    $this->add_script("salesman-index");
    $this->view('master/salesman/search')->render();
  }

  /**
   *
   */
  public function new_form()
  {
    $this->view("master/salesman/new")->render();
  }

  /**
   *
   */
  public function edit_form()
  {
    $this->render();
  }

  public function save()
  {

    $data = $this->_getPostData();
    $response = $this->request('put', 'salesman/save', $data);

    if ($response['error']) {
      $data['salesman'] = $response['data'];
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      $this->view("master/salesman/new")->render($data);
      return;
    }

    $message = $this->lang->line('Salesman.SLSM-0001');
    $this->set_message($message, 'info');

    redirect("salesman");
  }

  public function update()
  {
    $data = $this->_getPostData();
    $response = $this->request('post', 'salesman/update', $data);

    if ($response['error']) {
      $data['salesman'] = $response['data'];
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      $this->view("master/salesman/show")->render($data);
      return;
    }

    $message = $this->lang->line('Salesman.SLSM-0002');
    $this->set_message($message, 'info');

    redirect("salesman");
  }

  public function delete($id)
  {
    $response = $this->request('delete', 'salesman/' . $id . '/delete');

    if ($response['error']) {
      $message = $this->lang->line('Common.COMM-0004');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      redirect("salesman/". $id);
    }

    $message = $this->lang->line('Salesman.SLSM-0003');

    $this->set_message($message, 'info');

    redirect("salesman");
  }

  /**
   * Ajax search function called by Datatable
   * @return json data
   */
  public function search()
  {
    // default sort by col-idx-1
    $order_col_idx = $this->input->get("order[0][column]")?$this->input->get("order[0][column]"):1;
    $draw = $this->input->get("draw")?$this->input->get("draw"):1;

    // set order column name based on column idx
    if ((int)$order_col_idx == 0 ) {
      $order_col_name = "name";
    } elseif ((int)$order_col_idx == 1) {
      $order_col_name = "address";
    } elseif ((int)$order_col_idx == 2) {
      $order_col_name = "telp";
    }

    $order_col_dir = $this->input->get("order[0][dir]")?$this->input->get("order[0][dir]"):"asc";
    $offset = $this->input->get("start");
    $searchNameValue = $this->input->get("columns[0][search][value]");
    $searchAddressValue = $this->input->get("columns[1][search][value]");

    $data = [
      "salesman_name"             =>  $searchNameValue,
      "salesman_address"          =>  $searchAddressValue,
      "salesman_status"           =>  'A',
      "offset"                    =>  $offset,
      "limit"                     =>  $this->input->get("length"),
      "order_column"              =>  $order_col_name,
      "order_dir"                 =>  $order_col_dir,
      "draw"                      => $draw
    ];

    $response = $this->request("get", "salesman/search?" . http_build_query($data));

    $dt_rows = [];

    if ($response['statusCode'] == 200) {
      $dt_rows = [
        "draw"              => $draw + 1,
        "recordsTotal"      => (int) $response['recordsTotal'],
        "recordsFiltered"   => (int) $response['recordsFiltered'],
        "data"              => $response['data']
      ];
    }

    echo json_encode($dt_rows);
  }

  public function list_all()
  {

  }

  public function show($id)
  {
    $data = [];
    $response_salesman = $this->request('get', 'salesman/' . $id);
    $data['salesman'] = $response_salesman['data'];
    $this->add_script("salesman-show");
    $this->render($data);
  }

  private function _getPostData() {
    $id = $this->input->post("salesman_id");
    $name = $this->input->post("salesman_name");
    $code = $this->input->post("salesman_code");
    $address = $this->input->post("salesman_address");
    $telp = $this->input->post("salesman_telp");

    $data = [
      "salesman_id"           => $id,
      "salesman_name"         => $name,
      "salesman_code"         => $code,
      "salesman_address"      => $address,
      "salesman_telp"         => $telp
    ];

    return $data;
  }
}
