<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class OutletType extends Authenticated_Controller
{
  public function __construct()
  {
    parent::__construct();

    $this->layout('master');
  }

  public function index() {
    $this->add_script("outletType-index");
    $this->view('master/outletType/search')->render();
  }

  /**
   *
   */
  public function new_form()
  {
    $this->view("master/outletType/new")->render();
  }

  /**
   *
   */
  public function edit_form()
  {
    $this->render();
  }

  public function save()
  {
    $data = $this->_getPostData();

    $response = $this->request('put', 'outletType/save', $data);
    if ($response['error']) {
      $data['outletType'] = $response['data'];
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      $this->view("master/outletType/new")->render($data);
      return;
    }

    $message = $this->lang->line('OutletType.OLTP-0001');
    $this->set_message($message, 'info');

    redirect("outletType");
  }

  public function update()
  {
    $data = $this->_getPostData();

    $response = $this->request('post', 'outletType/update', $data);
    if ($response['error']) {
      $data['outletType'] = $response['data'];
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      $this->view("master/outletType/show")->render($data);
      return;
    }

    $message = $this->lang->line('OutletType.OLTP-0002');
    $this->set_message($message, 'info');

    redirect("outletType");
  }

  public function delete($id)
  {
    $response = $this->request('delete', 'outletType/' . $id . '/delete');
    if ($response['error']) {
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      redirect("outletType/" . $id);
    }

    $message = $this->lang->line('OutletType.OLTP-0003');
    $this->set_message($message, 'info');

    redirect("outletType");
  }

  /**
   * Ajax search function called by Datatable
   * @return json data
   */
  public function search()
  {
    // default sort by col-idx-1
    $order_col_idx = $this->input->get("order[0][column]")?$this->input->get("order[0][column]"):1;
    $draw = $this->input->get("draw")?$this->input->get("draw"):1;

    // set order column name based on column idx
    if ((int)$order_col_idx == 0 ) {
      $order_col_name = "code";
    } elseif ((int)$order_col_idx == 1) {
      $order_col_name = "name";
    }

    $order_col_dir = $this->input->get("order[0][dir]")?$this->input->get("order[0][dir]"):"asc";
    $offset = $this->input->get("start");
    $searchCodeValue = $this->input->get("columns[0][search][value]");
    $searchNameValue = $this->input->get("columns[1][search][value]");

    $data = [
      "outletType_code"           =>  $searchCodeValue,
      "outletType_name"           =>  $searchNameValue,
      "outletType_status"         =>  'A',
      "offset"                    =>  $offset,
      "limit"                     =>  $this->input->get("length"),
      "order_column"              =>  $order_col_name,
      "order_dir"                 =>  $order_col_dir,
      "draw"                      => $draw
    ];

    $response = $this->request("get", "outletType/search?" . http_build_query($data));

    $dt_rows = [];

    if ($response['statusCode'] == 200) {
      $dt_rows = [
        "draw"              => $draw + 1,
        "recordsTotal"      => (int) $response['recordsTotal'],
        "recordsFiltered"   => (int) $response['recordsFiltered'],
        "data"              => $response['data']
      ];
    }

    echo json_encode($dt_rows);
  }

  public function list_all()
  {

  }

  public function show($id)
  {
    $data = [];

    $response_outletType = $this->request('get', 'outletType/' . $id);

    $data['outletType'] = $response_outletType['data'];

    $this->add_script("outletType-show");

    $this->render($data);
  }

  private function _getPostData() {
    $id = $this->input->post("outletType_id");
    $name = $this->input->post("outletType_name");
    $code = $this->input->post("outletType_code");

    $data = [
      "outletType_id"       => $id,
      "outletType_name"     => $name,
      "outletType_code"     => $code
    ];

    return $data;
  }
}
