<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Outlet extends Authenticated_Controller
{
  public function __construct()
  {
    parent::__construct();

    $this->layout('master');
  }

  public function index() {
    $this->add_script("outlet-index");
    $this->view('master/outlet/search')->render();
  }

  /**
   *
   */
  public function new_form()
  {
    $data = [];

    $responseOutletArea = $this->request('get', 'area/list_all');
    $data['outletAreas'] = $responseOutletArea['data'];

    $responseOutletType = $this->request('get', 'outletType/list_all');
    $data['outletTypes'] = $responseOutletType['data'];

    $this->add_script("outlet-new");
    $this->view("master/outlet/new")->render($data);
  }

  public function save()
  {

    $data = $this->_getPostData();

    $response = $this->request('post', 'outlet/save', $data);
    if ($response['error']) {
      $data['outlet'] = $response['data'];
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      $responseOutletArea = $this->request('get', 'area/list_all');
      $data['outletAreas'] = $responseOutletArea['data'];

      $responseOutletType = $this->request('get', 'outletType/list_all');
      $data['outletTypes'] = $responseOutletType['data'];

      $this->add_script("outlet-new");
      $this->view("master/outlet/new")->render($data);
      return;
    }

    $message = $this->lang->line('Outlet.OUTL-0001');
    $this->set_message($message, 'info');

    redirect("outlet");
  }

  public function update()
  {
    $data = $this->_getPostData();

    $response = $this->request('post', 'outlet/update', $data);
    if ($response['error']) {
      $data['outlet'] = $response['data'];
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      $this->view("master/outlet/show")->render($data);
      return;
    }

    $message = $this->lang->line('Outlet.OUTL-0002');
    $this->set_message($message, 'info');

    redirect("outlet");
  }

  public function delete($id)
  {
    $response = $this->request('delete', 'outlet/' . $id . '/delete');
    if ($response['error']) {
      $data['outlet'] = $response['data'];
      $message = $this->lang->line('Common.COMM-0003');
      $message = $message . '<br/>' . $response['errorMessage'];
      $this->set_message($message, 'warning');

      redirect("outlet/". $id);
    }

    $message = $this->lang->line('Outlet.OUTL-0003');
    $this->set_message($message, 'info');

    redirect("outlet");
  }

  /**
   * Ajax search function called by Datatable
   * @return json data
   */
  public function search()
  {
    $order_col_idx = $this->input->get("order[0][column]")?$this->input->get("order[0][column]"):0;
    $order_col_name = "name"; // default column name (col-0)
    $draw = $this->input->get("draw")?$this->input->get("draw"):1;

    // set order column name based on column idx
    if ((int)$order_col_idx == 1 ) {
      $order_col_name = "address";
    }

    $order_col_dir = $this->input->get("order[0][dir]")?$this->input->get("order[0][dir]"):"asc";
    $offset = $this->input->get("start");
    $searchNameValue = $this->input->get("columns[0][search][value]");
    $searchAddressValue = $this->input->get("columns[1][search][value]");

    $data = [
      "outlet_name"      =>  $searchNameValue,
      "outlet_address"   =>  $searchAddressValue,
      "outlet_status"    =>  'A',
      "offset"              =>  $offset,
      "limit"               =>  $this->input->get("length"),
      "order_column"        =>  $order_col_name,
      "order_dir"           =>  $order_col_dir,
      "draw"                => $draw
    ];

    $response = $this->request("get", "outlet/search?" . http_build_query($data));

    $dt_rows = [];

    if ($response['statusCode'] == 200) {

      $dt_rows = [
        "draw"              => $draw + 1,
        "recordsTotal"      => (int) $response['recordsTotal'],
        "recordsFiltered"   => (int) $response['recordsFiltered'],
        "data"              => $response['data']
      ];
    }

    echo json_encode($dt_rows);
  }

  public function list_all()
  {

  }

  public function show($id)
  {
    $data = [];

    $response_outlet = $this->request('get', 'outlet/' . $id);

    $data['outlet'] = $response_outlet['data'];

    $responseOutletArea = $this->request('get', 'area/list_all');
    $data['outletAreas'] = $responseOutletArea['data'];

    $responseOutletType = $this->request('get', 'outletType/list_all');
    $data['outletTypes'] = $responseOutletType['data'];

    $this->add_script("outlet-show");
    $this->render($data);
  }

  private function _getPostData() {
    $id = $this->input->post("outlet_id");
    $code = $this->input->post("outlet_code");
    $name = $this->input->post("outlet_name");
    $address = $this->input->post("outlet_address");
    $area_id = $this->input->post("outlet_area_id");
    $type_id = $this->input->post("outlet_type_id");
    $telp = $this->input->post("outlet_telp");
    $npwp = $this->input->post("outlet_npwp");
    $npwp_name = $this->input->post("outlet_npwp_name");
    $npwp_address = $this->input->post("outlet_npwp_address");
    $pic = $this->input->post("outlet_pic");
    $acc_nos = $this->input->post("acc_no");
    $bank_infos = $this->input->post("bank_info");

    $bank_accs = [];
    for ($i = 0; $i < count($acc_nos); ++$i) {
      $bank_accs[] = [
        "outlet_account_no"    => $acc_nos[$i],
        "outlet_bank_info"     => $bank_infos[$i]
      ];
    }

    $data = [
      "outlet_id"           => $id,
      "outlet_code"         => $code,
      "outlet_name"         => $name,
      "outlet_address"      => $address,
      "outletarea_id"       => $area_id,
      "outlettype_id"       => $type_id,
      "outlet_telp"         => $telp,
      "outlet_npwp"         => $npwp,
      "outlet_npwp_name"    => $npwp_name,
      "outlet_npwp_address" => $npwp_address,
      "outlet_pic"          => $pic,
      "outlet_bank_accs"    => json_encode($bank_accs),
      "outlet_status"       => "A"
    ];

    return $data;
  }
}
