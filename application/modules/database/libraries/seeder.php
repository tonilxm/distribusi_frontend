<?php

/**
 * Class Seeder
 *
 * Provides a base class for creating seeders.
 */
class Seeder {

    public $error_string    = '';

    protected $ci;

    protected $db;
    protected $dbforge;

    //--------------------------------------------------------------------

    public function __construct ()
    {
        $this->ci =& get_instance();

        $this->ci->load->library('Cli');
        $this->ci->load->dbforge();

        $this->db       = $this->ci->db;
        $this->dbforge  = $this->ci->dbforge;
    }

    //--------------------------------------------------------------------


    /**
     * Run the database seeds
     */
    public function run ()
    {

    }

    //--------------------------------------------------------------------

    /**
     * Loads the class file and calls the run() method
     * on the class.
     *
     * @param $class
     */
    public function call ($class)
    {
        $path = APPPATH .'database/seeds/'. str_replace('.php', '', $class) .'.php';

        if ( ! is_file($path))
        {
            return Cli::write( Cli::color("\tUnable to find seed class: ". $class, 'red') );
        }

        require $path;

        $seeder = new $class();

        $seeder->run();

        unset($seeder);

        return Cli::write( Cli::color("\tSeeded: $class", 'green') );
    }

    //--------------------------------------------------------------------

}