<?php

class Database extends CI_Controller {

    //--------------------------------------------------------------------

    public function __construct ()
    {
        parent::__construct();

        $this->load->library('Cli');
    }

    //--------------------------------------------------------------------

    /**
     * Provides a command-line interface to the migration scripts.
     * If no $to is provided, will migrate to the latest version.
     *
     * Example:
     *      > php index.php database migrate
     *
     * @param null $to
     */
    public function migrate ($to=null)
    {
        $this->load->library('migration');

        // Get our stats on the migrations
        $latest = $this->migration->get_latest();
        $latest = empty($latest) ? 0 : $latest;

        if ( empty($latest))
        {
            return Cli::write( CLI::color("\tNo migrations found.", 'yellow') );
        }

        $current = $this->migration->get_version();

        // Do the migration!
        if ( is_null($to))
        {
            if ( ! $this->migration->latest() )
            {
                return Cli::write( CLI::color("\n\tERROR: ". $this->migration->error_string() ."\n", 'red') );
            }
        }
        else
        {
            if ( ! $this->migration->version($to) )
            {
                return Cli::write( CLI::color("\n\tERROR: ". $this->migration->error_string() ."\n", 'red') );
            }
        }

        Cli::write( CLI::color("\n\tSuccessfully migrated database from version {$current} to {$latest}.\n", 'green') );
    }

    //--------------------------------------------------------------------

    /**
     * Migrates the database back to 0, then back up to the latest version.
     */
    public function refresh ()
    {
        $this->load->library('migration');

        if ( ! $this->migration->version(0) )
        {
            return Cli::write( CLI::color("\tERROR: ". $this->migration->error_string(), 'red') );
        }

        Cli::write( CLI::color("\tCleared the database.", 'green') );

        if ( ! $this->migration->latest() )
        {
            return Cli::write( CLI::color("\tERROR: ". $this->migration->error_string(), 'red') );
        }

        Cli::write( CLI::color("\tRe-installed the database to the latest migration.", 'green') );
    }

    //--------------------------------------------------------------------

    /**
     * Creates a new migration file ready to be used.
     *
     * @param $name
     */
    public function newMigration ($name=null)
    {
        if ( empty($name))
        {
            return Cli::write( Cli::color("\tYou must provide a migration name (no quotes).", 'red') );
        }

        $this->load->library('migration');

        $version = $this->migration->get_latest() + 1;

        $file = str_pad($version, 3, '0', STR_PAD_LEFT) .'_'. $name .'.php';

        $path = APPPATH .'database/migrations/'. $file;

        $contents =<<<EOT
<?php

class Migration_{name} extends CI_Migration {

    public function up ()
    {

    }

    //--------------------------------------------------------------------

    public function down ()
    {

    }

    //--------------------------------------------------------------------

}
EOT;
        $contents = str_replace('{name}', $name, $contents);

        $this->load->helper('file');

        if (write_file($path, $contents))
        {
            return Cli::write( Cli::color("\tNew migration created: ". Cli::color($file, 'yellow'), 'green') );
        }

        return Cli::write( Cli::color("\tUnkown error trying to create migration: {$file}", 'red') );
    }

    //--------------------------------------------------------------------

    /**
     * Installs any database seeds stored in database/seeds
     */
    public function seed ($name='DatabaseSeeder')
    {
        $this->load->library('seeder');

        $this->seeder->call($name);
    }

    //--------------------------------------------------------------------

}