<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * A basic user_model that handles most of the features you need to get
 * started supporting a simple user system for your application.
 *
 * SQL:
 *     CREATE TABLE `users` (
 *         `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 *         `username` varchar(60) DEFAULT NULL,
 *         `email` varchar(255) NOT NULL DEFAULT '',
 *         `password_hash` varchar(60) NOT NULL,
 *         `last_login` datetime DEFAULT NULL,
 *         `created_on` datetime DEFAULT NULL,
 *         `modified_on` datetime DEFAULT NULL,
 *         PRIMARY KEY (`id`),
 *         UNIQUE KEY `email` (`email`),
 *         UNIQUE KEY `username` (`username`)
 *     ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 */
class User_model extends MY_Model {

    protected $_table               = 'users';

    protected $before_insert        = array('hash_password', 'create_username', 'handle_papers');
    protected $after_insert         = array('handle_papers_after');
    protected $before_update        = array('hash_password');

    protected $date_format          = 'datetime';

    protected $protected_attributes = array('submit', 'id');

    protected $validation_rules = array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|max_length[60]|xss_clean'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|max_length[255]|valid_email|xss_clean'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|min_length[8]|max_length[72]'
        ),
        array(
            'field' => 'pass_confirm',
            'label' => 'Password (again)',
            'rules' => 'matches[password]'
        ),
        array(
            'field' => 'background',
            'label' => 'Short Background',
            'rules' => 'strip_tags|trim|xss_clean'
        ),
        array(
            'field' => 'changes',
            'label' => 'What Would You Change',
            'rules' => 'strip_tags|trim|xss_clean'
        ),
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|max_length[72]|xss_clean'
        ),
        array(
            'field' => 'work_email',
            'label' => 'Work Email',
            'rules' => 'trim|max_length[255]|valid_email|xss_clean'
        ),
    );

    protected $insert_validation_rules = array(
        'username'  => 'required|is_unique[users.username]',
        'email'     => 'required|is_unique[users.email]',
        'password'  => 'required',
        'pass_confirm'  => 'required',
    );

    protected $paper_id = null;

    //--------------------------------------------------------------------

    public function insert($data, $skip_validation=FALSE)
    {
        $background     = isset($data['background']) ? $data['background'] : null;
        $changes        = isset($data['changes'])    ? $data['changes'] : null;
        unset($data['background'], $data['changes']);

        $user_id = parent::insert($data, $skip_validation);

        if ( ! $user_id)
        {
            return FALSE;
        }

        $this->handle_profiles( array('background'  => $background,
                                      'changes'     => $changes,
                                      'user_id'     => $user_id));

        return $user_id;
    }

    //--------------------------------------------------------------------


    /**
     * Uses PHPPass to create a secure password hash of any password.
     * This requires that a field named 'password_hash' be present in the
     * database as a VARCHAR(60) column.
     *
     * To function properly, you should send a field called 'password' in
     * the $data array.
     *
     * @param  array $data
     */
    public function hash_password($data)
    {
        if ( ! isset($data['password']))
        {
            return $data;
        }

        $hash = $this->do_hash_password($data['password']);

        $data['password_hash'] = $hash;
        unset($data['password'], $data['pass_confirm']);

        return $data;
    }

    //--------------------------------------------------------------------

    public function do_hash_password($password)
    {
        // Load the password hash library
        if (!class_exists('PasswordHash'))
        {
            require(APPPATH .'libraries/PasswordHash.php');
        }
        $hasher = new PasswordHash(8, false);

        // Passwords should never be longer than 72 characters to prevent DOS attacks
        if (strlen($password) > 72) die('Password must be 72 characters or less. Possible DOS attack.');

        $hash = $hasher->HashPassword($password);
        unset($hasher);

        if (strlen($hash) < 20)
        {
            // Something went wrong....
            die('Something terribly wrong happened while hashing the password.');
        }

        return $hash;
    }

    //--------------------------------------------------------------------

    /**
     * To be used prior to inserting a new user, this function will set
     * the username to be equal to the email if it hasn't already been set.
     *
     * @param  array $data
     */
    public function create_username($data)
    {
        if (isset($data['username']))
        {
            return $data;
        }

        // Nothing we can do without the email.
        if (!isset($data['email']))
        {
            return $data;
        }

        if (!isset($data['username']) || (isset($data['username']) && empty($data['username'])))
        {
            $data['username'] = $data['email'];
        }

        return $data;
    }

    //--------------------------------------------------------------------

    /**
     * Strips out the topic id from the $data var since it's not being
     * saved to the user table. Then saves/updates it in the appropriate tables.
     *
     */
    public function handle_papers($data)
    {
        if ( ! isset($data['paper_id']) )
        {
            return $data;
        }

        $this->paper_id = $data['paper_id'];
        unset($data['paper_id']);

        return $data;
    }

    //--------------------------------------------------------------------

    public function handle_papers_after($user_id)
    {
        $paper_id = $this->paper_id;

        if ( ! $paper_id)
        {
            return $user_id;
        }

        $this->load->model('papers/paper_model');
        $this->paper_model->link_user($paper_id, $user_id);

        $this->paper_id = null;

        return $user_id;
    }

    //--------------------------------------------------------------------


    public function handle_profiles($data)
    {
        if ( ! isset($data['background']) &&  ! isset($data['changes']))
        {
            return $data;
        }

        $new_data = array();

        $background     = isset($data['background']) ? $data['background'] : null;
        $changes        = isset($data['changes'])    ? $data['changes'] : null;
        $user_id        = isset($data['user_id'])    ? $data['user_id'] : $this->auth->user_id();

        if ( ! $this->save_user_profile($user_id, $background, $changes))
        {
            $this->error = 'Error saving profile information. '. $this->get_db_error_message('dbw');
        }

        unset($data['background'], $data['changes']);

        return $data;
    }

    //--------------------------------------------------------------------

    public function save_user_profile($user_id, $background=null, $changes=null)
    {
        // Does one exist already?
        $count = $this->db->where('user_id', $user_id)
                          ->count_all_results('user_profiles');

        $user_id = (int)$user_id;

        $data = array(
            'background' => $background,
            'changes'   => $changes
        );

        if ($count)
        {
            $this->db->where('user_id', $user_id)
                     ->update('user_profiles', $data);
        }
        else
        {
            $data['user_id'] = $user_id;

            $this->db->insert('user_profiles', $data);
        }

        return (bool)$this->db->affected_rows();
    }

    //--------------------------------------------------------------------

    public function get_user_profile($user_id, $fields=array())
    {
        if (count($fields))
        {
            $this->db->select( implode(',', $fields) );
        }

        $query = $this->db->where('user_id', $user_id)
                          ->get('user_profiles');

        if ( ! $query->num_rows())
        {
            return NULL;
        }

        return $query->row();
    }

    //--------------------------------------------------------------------

    public function with_profile()
    {
        $this->db->join('user_profiles', 'user_profiles.user_id = users.id', 'left');

        return $this;
    }

    //--------------------------------------------------------------------

    public function in_moderation()
    {
        $this->db->where('in_moderation', 1);

        return $this;
    }

    //--------------------------------------------------------------------

    public function unmoderated()
    {
        $this->db->where('in_moderation', 0);

        return $this;
    }

    //--------------------------------------------------------------------


    public function approve_moderation($user_id, $role=ROLE_USER)
    {
        $data = array(
            'in_moderation' => 0,
            'banned'        => 0,
            'role_id'       => $role
        );

        $this->update($user_id, $data);

        return (bool)$this->db->affected_rows();
    }

    //--------------------------------------------------------------------

    public function deny_moderation($user_id, $message='')
    {
        $data = array(
            'in_moderation'     => 0,
            'banned'            => 1,
            'moderation_msg'    => $message
        );

        $this->update($user_id, $data);

        return (bool)$this->db->affected_rows();
    }

    //--------------------------------------------------------------------

    public function this_month ()
    {
        $start_time = date('Y-m-01 00:00:00');
        $end_time   = date('Y-m-t 23:59:59');

        $this->where('created_at' >= $start_time);
        $this->where('created_at' <= $end_time);

        return $this;
    }

    //--------------------------------------------------------------------

    //--------------------------------------------------------------------
    // Login History Methods
    //--------------------------------------------------------------------

    public function get_history()
    {
        $query = $this->db->select('user_logins.*, users.username, users.email')
                          ->join('users', 'user_logins.user_id = users.id', 'left')
                          ->order_by('timestamp', 'desc')
                          ->get('user_logins');

        if ( ! $query->num_rows())
        {
            return null;
        }

        return $query->result();
    }

    //--------------------------------------------------------------------

}