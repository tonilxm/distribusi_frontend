<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {

    protected $model_file = 'users/user_model';

    //--------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();

        $this->load->driver('Auth');
    }

    //--------------------------------------------------------------------

    public function login()
    {
        $this->load->library('form_validation');

        $redirect = $this->input->post('redirect');

        // Any chance it was set in the session?
        if ($this->session->userdata('after_login'))
        {
            $redirect = $this->session->userdata('after_login');
            $this->session->unset_userdata('after_login');
        }

        if ($redirect == site_url() || empty($redirect))
        {
            $redirect .= 'dashboard';
        }

        $this->set_var('redirect', $redirect);

        if ($this->input->post('submit'))
        {
            $remember = (boolean)$this->input->post('remember');

            $data = array(
                'username' => $this->input->post('username', true),
                'password' => $this->input->post('password')
            );

            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

            if ($this->form_validation->run())
            {
                if ($result = $this->auth->login($data, $remember))
                {
                    redirect($redirect);
                }
            }

            $errors = validation_errors();

            if (empty($errors))
            {
                $this->set_message('Invalid Email/Password combination.', 'alert');
            }
        }

        $this->render();
    }

    //--------------------------------------------------------------------

    public function logout()
    {
        $redirect = site_url();

        $this->auth->logout($redirect);
    }

    //--------------------------------------------------------------------


    public function register()
    {
        $this->load->library('form_validation');

        if ($this->input->post('submit'))
        {
            if ($this->user_model->insert($this->input->post()))
            {
                $this->set_message('Your account has been created. Please login.');
                redirect('/login');
            }
        }

        $this->load->model('papers/paper_model');
        $papers = $this->paper_model->select('id, title')
                                    ->order_by('title', 'asc')
                                    ->find_all();
        $this->set_var('papers', $papers);

        $this->render();
    }

    //--------------------------------------------------------------------

    public function profile()
    {
        $this->auth->restrict();

        $this->load->helper('form');

        if ($this->input->post('submit_send_code'))
        {
            $this->send_verification_code();
        }

        if ($this->input->post('submit_code'))
        {
            $this->verify_code( $this->input->post('code') );
        }

        if ($this->input->post('submit_profile'))
        {
            $data = array(
                'email'         => $this->input->post('email'),
                'work_email'    => $this->input->post('work_email'),
                'background'    => $this->input->post('background'),
                'changes'       => $this->input->post('changes')
            );

            if ($this->user_model->update($this->auth->user_id(), $data))
            {
                $this->set_message('Your profile has been saved.', 'success');
            }
            else
            {
                if ( ! validation_errors())
                {
                    $this->set_message('There was an error saving your profile. Please try again later.');
                }
            }
        }

        $user = $this->user_model->with_profile()
                                 ->find( $this->auth->user_id() );

        $this->set_var('user', $user);

        $this->load->model('papers/paper_model');
        $this->set_var('papers', $this->paper_model->find_for_user($this->auth->user_id()));

        $this->render();
    }

    //--------------------------------------------------------------------

    public function forgot_password()
    {
        $this->load->library('form_validation');

        $errors = array();

        $email = $this->input->post('email', TRUE);

        if ($this->input->post('submit'))
        {
            // Empty email?
            if (empty($email))
            {
                $errors[] = 'The email address cannot be blank.';
            }
            else if ( ! filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                $errors[] = 'You must provide a valid email address.';
            }
            else if ( ! $user = $this->user_model->find_by('email', $email))
            {
                $errors[] = 'Unable to find an account with that email address.';
            }

            if ( ! count($errors))
            {
                if ( $this->send_forgotten_email($user))
                {
                    $this->set_var('email_sent', true);
                }
                else
                {
                    $this->set_var('email_sent', false);
                }
            }

            $this->set_var('errors', implode('<br/>', $errors));
        }

        $this->render();
    }

    //--------------------------------------------------------------------

    /**
     * Verifies the users' code and allows them to choose
     * a new password.
     *
     * @param  [type] $code [description]
     * @return [type]       [description]
     */
    public function reset_password($code=null)
    {
        $this->load->library('form_validation');

        if ($this->input->post('submit'))
        {
            // Grab our user so that we can verify, once again,
            // that the user and the code match up.
            $email = $this->input->post('email', TRUE);
            $code = $this->input->post('code', TRUE);

            if ( ! $user = $this->user_model->find_by( array('email'=>$email, 'reset_hash'=>$code) ))
            {
                die('Unable to save new password. Possible hacking attempt.');
            }

            $this->user_model->require_field('password');
            $this->user_model->require_field('pass_confirm');

            $data = array(
                'password'      => $this->input->post('password'),
                'pass_confirm'  => $this->input->post('pass_confirm')
            );

            if ($this->user_model->update($user->id, $data))
            {
                $this->set_message('Your password was successfully reset. Please login below.', 'success');
                return redirect('login');
            }
        }

        // If we don't have a reset code, we can't
        // allow them to reset since we don't know for who
        // and if they should be allowed.
        $can_reset = false;

        $email = '';

        if ( ! is_null($code) && $user = $this->user_model->find_by('reset_hash', $code))
        {
            $email = $user->email;
            $can_reset = true;
        }

        $view_data = array(
            'can_reset' => $can_reset,
            'code'      => $code,
            'email'     => $email
        );
        $this->set_var($view_data);

        $this->render();
    }

    //--------------------------------------------------------------------

    public function dashboard()
    {
        $this->auth->restrict();

        // Get the users papers.
        $this->load->model('papers/paper_model');
        $this->set_var('papers', $this->paper_model->find_for_user($this->auth->user_id()) );

        $this->load->helper('typography');

        $this->render();
    }

    //--------------------------------------------------------------------

    public function recommend()
    {
        $this->auth->restrict();

        $this->load->library('form_validation');

        if ($this->input->post('submit'))
        {
            /*
                Validate it.
             */
            $this->form_validation->set_rules('name', 'Name', 'required|trim|min_length[2]|max_length[72]|xss_clean');
            $this->form_validation->set_rules('job', 'Job', 'required|trim|min_length[2]|max_length[255]|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');

            if ($this->form_validation->run())
            {
                /*
                    Email it.
                 */
                $data = array(
                    'topic_name'    => $this->input->post('topic_name'),
                    'name'          => $this->input->post('name'),
                    'job'           => $this->input->post('job'),
                    'site_name'     => $this->config->item('site.name'),
                    'site_link'     => site_url(),
                    'users_name'    => $this->auth->current_user()->name
                );

                $message = $this->load->view('emails/recommend_user', $data, true);

                $to = $this->input->post('email');

                $this->load->library('email');

                $this->email->to($to);
                $this->email->from($this->config->item('site.email'));
                $this->email->subject($data['users_name'] .' thinks your opinion is needed!');
                $this->email->message($message);

                if ($this->email->send())
                {
                    $this->set_message('An email was just sent with your recommendation. Thanks for building the community!', 'success');
                    redirect('recommend');
                }

                if (ENVIRONMENT == 'development')
                {
                    echo $this->email->print_debugger();
                }

                $this->set_message('There was an error sending the email. Please email us the recommendation at recommend@pin.org.', 'error');
            }
        }

        $this->render();
    }

    //--------------------------------------------------------------------

    /**
     * Shows the user their bookmarked posts and comments.
     */
    public function bookmarks($paper_id=0)
    {
        $this->auth->restrict();
        $this->load->model('bookmark_model');
        $this->load->helper('typography');

        $user_id = $this->auth->user_id();

        $this->set_var('bookmarks', $this->bookmark_model->get_user_overview($user_id, $paper_id));

        $this->render();
    }

    //--------------------------------------------------------------------


    //--------------------------------------------------------------------
    // Private Methods
    //--------------------------------------------------------------------

    /**
     * Sends a verification code to the user so they can verify
     * their work email address.
     *
     * @return void
     */
    private function send_verification_code()
    {
        $this->load->helper('string');
        $code = random_string('alnum', 16);

        /*
            Save to database
         */
        $data = array(
            'name'              => $this->input->post('name'),
            'work_email'        => $this->input->post('work_email'),
            'activation_code'   => $code,
            'activation_sent'   => date('Y-m-d H:i:s')
        );

        $this->user_model->require_field( array('name', 'work_email') );

        if ( ! $this->user_model->update($this->auth->user_id(), $data))
        {
            if ( ! validation_errors())
            {
                $this->set_message('Unable to send code. '. $this->user_model->get_db_error_message('dbw'), 'alert');
            }
            return;
        }

        /*
            Send the email
         */
        $this->load->library('email');

        $this->email->to($data['work_email']);
        $this->email->from( $this->config->item('site.email') );
        $this->email->subject('Please Verify Your Account');

        $email_data = array(
            'site_name'     => $this->config->item('site.name'),
            'site_email'    => $this->config->item('site.email'),
            'users_name'    => $this->input->post('name'),
            'users_email'   => $this->input->post('work_email'),
            'profile_link'  => site_url('/profile'),
            'code'          => $code
        );

        $this->email->message( $this->load->view('emails/send_code', $email_data, true) );

        if ($this->email->send())
        {
            $this->set_message('An email has been sent to your work email address that contains your verification code. Enter it below.', 'success');
            return;
        }

        if (ENVIRONMENT == 'development')
        {
            echo $this->email->print_debugger();
        }

        $this->set_message('There was an error sending the email. Please contact the system admin to get your account verified manually.', 'error');
    }

    //--------------------------------------------------------------------

    /**
     * Checks the provided code against the user's code.
     * @param  [type] $code [description]
     * @return [type]       [description]
     */
    private function verify_code($code)
    {
        if (empty($code) || $code != $this->auth->current_user()->activation_code)
        {
            $this->set_message('The provided activation code does not match what we have on record. Please check your entry and try again.', 'alert');
            redirect('/profile');
        }

        // We're here - so it's a match. Remove the code so it can't be used again
        // set our flag to validated and change our user role

        $this->set_message('Thank you for verifying your code. Your account is not activated and you have full access to the site.', 'success');

        $data = array(
            'activation_code'   => NULL,
            'verified'          => 1,
            'role_id'           => ROLE_VERIFIED_USER
        );

        $this->user_model->update($this->auth->user_id(), $data);
        redirect('/profile');
    }

    //--------------------------------------------------------------------

    /**
     * Sends the email to the user with the link back here to reset their
     * password.
     *
     * @param  object $user The user object
     *
     * @return void
     */
    private function send_forgotten_email($user)
    {
        $this->load->helper('string');
        $code = random_string('unique');

        /*
            Save to database
         */
        $data = array(
            'reset_hash'    => $code
        );

        if ( ! $this->user_model->update($user->id, $data))
        {
            if ( ! validation_errors())
            {
                $this->set_message('Unable to send code. '. $this->user_model->get_db_error_message('dbw'), 'alert');
            }
            return FALSE;
        }

        /*
            Send the email
         */
        $this->load->library('email');

        $this->email->to($user->email);
        $this->email->from( $this->config->item('site.email') );
        $this->email->subject('Password Reset Instructions');

        $email_data = array(
            'site_name'     => $this->config->item('site.name'),
            'site_email'    => $this->config->item('site.email'),
            'users_name'    => $this->input->post('name'),
            'users_email'   => $this->input->post('email'),
            'reset_link'    => site_url('/reset_password'),
            'code'          => $code
        );

        $this->email->message( $this->load->view('emails/reset_password', $email_data, true) );

        if ($this->email->send())
        {
            $this->set_message('An email has been sent to your personal email address with instructions on resetting your password.', 'success');
            return TRUE;
        }

        if (ENVIRONMENT == 'development')
        {
            echo $this->email->print_debugger();
        }

        $this->set_message('There was an error sending the email. Please contact the system admin to get your account verified manually.', 'error');

        return FALSE;
    }

    //--------------------------------------------------------------------

}