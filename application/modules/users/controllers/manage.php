<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
    Users Module - Manage controller

    maps to /manage/users...
 */
class Manage extends Authenticated_Controller {

    protected $restrict_to  = 'admin';

    protected $model_file   = 'users/user_model';

    protected $use_layout   = 'admin';

    protected $debug        = false;

    //--------------------------------------------------------------------

    public function index()
    {
        $users = $this->user_model->unmoderated()
                                  ->limit(25)
                                  ->find_all();

        $data = array(
            'users'         => $users,
            'total_users'   => $this->user_model->count_all(),
            'roles'         => $this->auth->roles()
        );

        $this->set_var('pageTitle', 'Users');
        $this->render($data);
    }

    //--------------------------------------------------------------------

    public function create()
    {
        $this->load->library('form_validation');

        if ($this->input->post('submit'))
        {
            $data = array(
                'name'          => $this->input->post('name'),
                'username'      => $this->input->post('username'),
                'email'         => $this->input->post('email'),
                'work_email'    => $this->input->post('work_email'),
                'role_id'       => $this->input->post('role_id'),
                'password'      => $this->input->post('password'),
                'pass_confirm'  => $this->input->post('pass_confirm')
            );

            if ($this->user_model->insert($data))
            {
                $this->set_message('User successfully created.');
                redirect('manage/users');
            }
        }

        $this->set_var('roles', $this->auth->roles());
        $this->set_var('pageTitle', 'Create User');
        $this->render();
    }

    //--------------------------------------------------------------------

    public function edit($id=0)
    {
        $this->load->library('form_validation');

        if ($this->input->post('submit'))
        {
            $data = array(
                'name'          => $this->input->post('name'),
                'username'      => $this->input->post('username'),
                'email'         => $this->input->post('email'),
                'work_email'    => $this->input->post('work_email'),
                'role_id'       => $this->input->post('role_id'),
            );

            if ($this->input->post('password') && $this->input->post('pass_confirm'))
            {
                $data['password']       = $this->input->post('password');
                $data['pass_confirm']   = $this->input->post('pass_confirm');
            }

            if ($this->user_model->update($id, $data))
            {
                $this->user_model->save_user_profile($id, $this->input->post('background'), $this->input->post('changes'));

                $this->set_message('User successfully saved.');
                redirect('manage/users');
            }
        }

        $data = array(
            'user'      => $this->user_model->find($id),
            'profile'   => $this->user_model->get_user_profile($id),
            'roles'     => $this->auth->roles(),
            'paper_form'=> $this->render_paper_form(array(), $id)
        );

        if ( ! $data['user'])
        {
            $this->set_message('User Not Found.', 'alert');
            redirect('/manage/users');
        }

        $this->set_var('pageTitle', 'Edit User');
        $this->render($data);
    }

    //--------------------------------------------------------------------

    public function moderate()
    {
        $users = $this->user_model->in_moderation()
                                  ->find_all();

        $this->set_var('users', $users);
        $this->render();
    }

    //--------------------------------------------------------------------

    /**
     * Removes the moderation flag, emails them to let them know, and
     * returns us to the Queue.
     *
     * @param  int $id The ID of the user to approve
     */
    public function moderate_approve($id)
    {
        // make sure the user even exists.
        $user = $this->user_model->find_by( array('id' => $id, 'in_moderation' => 1) );

        if ( ! $user)
        {
            $this->set_message('Unable to find a user in moderation with ID #'. $id, 'alert');
            redirect('/manage/users/moderate');
        }

        // Remove the moderation flag.
        if  ( ! $this->user_model->approve_moderation($id))
        {
            $this->set_message('Error approving this member. '. $this->user_model->get_db_error_message(), 'alert' );
            redirect('/manage/users/moderate');
        }

        $this->send_moderation_email($user, 'approve');

        $this->set_message('User was approved.', 'success');
        redirect('/manage/users/moderate');
    }

    //--------------------------------------------------------------------

    /**
     * Denies someone access to the site. Effectively sends them a denial
     * email and bans them.
     *
     * @param  int $id The ID of the user to deny
     */
    public function moderate_deny($id)
    {
        // make sure the user even exists.
        $user = $this->user_model->find_by( array('id' => $id, 'in_moderation' => 1) );

        if ( ! $user)
        {
            $this->set_message('Unable to find a user in moderation with ID #'. $id, 'alert');
            redirect('/manage/users/moderate');
        }

        // Remove the moderation flag.
        if  ( ! $this->user_model->deny_moderation($id))
        {
            $this->set_message('Error denying this member. '. $this->user_model->get_db_error_message(), 'alert' );
            redirect('/manage/users/moderate');
        }

        $this->send_moderation_email($user, 'deny');

        $this->set_message('User was denied.', 'success');
        redirect('/manage/users/moderate');
    }

    //--------------------------------------------------------------------

    public function modify_papers($user_id)
    {
        $return = array(
            'fragments' => array()
        );

        $this->load->model('papers/paper_model');
        $this->load->helper('form');

        $paper_id = $this->input->post('paper_id');

        $data = array(
            'error' => false
        );

        // Is the user already attached to this paper?
        if ($this->paper_model->user_linked_to_paper($paper_id, $user_id) === true)
        {
            $data['error'] = 'User already linked to this paper.';
        }
        else
        {
            $this->paper_model->link_user($paper_id, $user_id);
        }

        $return['fragments']['#paper-form'] = $this->render_paper_form($data, $user_id);

        $this->render_json($return);
    }

    //--------------------------------------------------------------------

    public function unlink_paper($paper_id, $user_id)
    {
        $return = array(
            'fragments' => array()
        );

        $this->load->model('papers/paper_model');
        $this->load->helper('form');

        $this->paper_model->unlink_user($paper_id, $user_id);

        $return['fragments']['#paper-form'] = $this->render_paper_form(array(), $user_id);

        $this->render_json($return);
    }

    //--------------------------------------------------------------------


    /*
        Displays the pform for papers in the user edit screen.
     */
    private function render_paper_form($data=array(), $user_id=0)
    {
        $this->load->model('papers/paper_model');

        $data['papers']         = $this->paper_model->list_titles();
        $data['user_papers']    = $this->paper_model->list_for_user($user_id);
        $data['user_id']        = $user_id;

        return $this->load->view('manage/paper_form', $data, true);
    }

    //--------------------------------------------------------------------


    //--------------------------------------------------------------------
    // Ajax Methods
    //--------------------------------------------------------------------

    public function ban($user_id)
    {
        $return_url = $this->input->server('HTTP_REFERER');

        if ( ! $user_id)
        {
            $this->set_message('Unable to locate the user to ban.');
            redirect( $return_url );
        }

        if ( ! $this->user_model->update($user_id, array('banned'=>1)) )
        {
            $this->set_message('Unable to ban the user. '. $this->user_model->get_db_error_message('dbw'));
        }
        else
        {
            $this->set_message('The user was banned.', 'success');
        }

        redirect($return_url);
    }

    //--------------------------------------------------------------------

    public function unban($user_id)
    {
        $return_url = $this->input->server('HTTP_REFERER');

        if ( ! $user_id)
        {
            $this->set_message('Unable to locate the user to unban.');
            redirect( $return_url );
        }

        if ( ! $this->user_model->update($user_id, array('banned'=>0)) )
        {
            $this->set_message('Unable to unban the user. '. $this->user_model->get_db_error_message('dbw'));
        }
        else
        {
            $this->set_message('The user was un-banned.', 'success');
        }

        redirect($return_url);
    }

    //--------------------------------------------------------------------

    public function delete($user_id)
    {
        $return_url = $this->input->server('HTTP_REFERER');

        if ( ! $user_id)
        {
            $this->set_message('Unable to locate the user to delete.');
            redirect( $return_url );
        }

        if ( ! $this->user_model->delete($user_id) )
        {
            $this->set_message('Unable to delete the user. '. $this->user_model->get_db_error_message('dbw'));
        }
        else
        {
            $this->set_message('The user was deleted.', 'success');
        }

        redirect($return_url);
    }

    //--------------------------------------------------------------------

    public function recommend()
    {
        $this->load->library('form_validation');

        if ($this->input->post('submit'))
        {
            /*
                Validate it.
             */
            $this->form_validation->set_rules('name', 'Name', 'required|trim|min_length[2]|max_length[72]|xss_clean');
            $this->form_validation->set_rules('job', 'Job', 'required|trim|min_length[2]|max_length[255]|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');

            if ($this->form_validation->run())
            {
                /*
                    Email it.
                 */
                $data = array(
                    'topic_name'    => $this->input->post('topic_name'),
                    'name'          => $this->input->post('name'),
                    'job'           => $this->input->post('job'),
                    'site_name'     => $this->config->item('site.name'),
                    'site_link'     => site_url(),
                    'users_name'    => $this->auth->current_user()->name
                );

                $message = $this->load->view('emails/recommend_user', $data, true);

                $to = $this->input->post('email');

                $this->load->library('email');

                $this->email->to($to);
                $this->email->from($this->config->item('site.email'));
                $this->email->subject($data['users_name'] .' thinks your opinion is needed!');
                $this->email->message($message);

                if ($this->email->send())
                {
                    $this->set_message('An email was just sent with your recommendation. Thanks for building the community!', 'success');
                    redirect('recommend');
                }

                if ($this->debug)
                {
                    echo $this->email->print_debugger();
                }

                $this->set_message('There was an error sending the email. Please verify your email settings.', 'error');
            }
        }

        $this->render();
    }

    //--------------------------------------------------------------------

    public function history()
    {
        $limit = 25;
        $offset = $this->uri->segment(4);

        $logins = $this->user_model->limit($limit, $offset)
                                   ->get_history();

        $this->set_var('logins', $logins);

        $total_rows = $this->db->count_all('user_logins');

        $this->set_var('range_first', (int)$offset + 1);
        $this->set_var('range_end', (int)$offset + $limit > $total_rows ? $total_rows : (int)$offset + $limit);
        $this->set_var('total_rows', $total_rows);

        $this->load->library('pagination');

        $config['base_url']     = site_url('manage/users/history');
        $config['total_rows']   = $total_rows;
        $config['per_page']     = $limit;

        $this->pagination->initialize($config);

        $this->set_var('pageTitle', 'Login History');
        $this->render();
    }

    //--------------------------------------------------------------------

    /**
     * Sends both the approved and denied emails for moderation.
     *
     * @param  object $user The user object.
     * @param  string $type Either 'approve' or 'deny'
     * @return [type]       [description]
     */
    public function send_moderation_email($user, $type)
    {
        /*
            Send the email
         */
        $this->load->library('email');

        $this->email->to($user->email);
        $this->email->from( $this->config->item('site.email') );
        $this->email->subject('Update to Your Account Status');

        $email_data = array(
            'site_name'     => $this->config->item('site.name'),
            'site_email'    => $this->config->item('site.email'),
            'users_email'   => $user->email,
            'site_link'     => site_url(),
        );

        $this->email->message( $this->load->view('emails/moderate_'. $type, $email_data, true) );

        if ($this->email->send())
        {
            return TRUE;
        }

        if (ENVIRONMENT == 'development')
        {
//            echo $this->email->print_debugger();
        }

        $this->set_message('There was an error sending the email. Please contact the system admin.', 'error');

        return FALSE;
    }

    //--------------------------------------------------------------------
}