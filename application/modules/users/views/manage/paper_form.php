<div id="paper-form">
<?= form_open( site_url('manage/users/modify_papers/'. $user_id), 'class="custom ajax"' ); ?>

    <div class="row">
        <div class="large-3 columns">
            <label for="paper_id" class="right align">Add Paper</label>
        </div>
        <div class="large-7 columns">
            <select name="paper_id">
                <option value="0">Select a paper...</option>
            <?php if (is_array($papers) && count($papers)) : ?>
                <?php foreach ($papers as $paper) : ?>
                    <option value="<?= $paper->id ?>"><?php e($paper->title) ?></option>
                <?php endforeach; ?>
            <?php else: ?>
                <option>No published papers.</option>
            <?php endif; ?>
            </select>
        </div>
        <div class="large-2 columns">
            <input type="submit" name="submit" class="button small" value="Add Paper" />
        </div>
    </div>

    <br/>
    <table style="width: 100%">
        <thead>
            <tr>
                <th>Title</th>
                <th style="width: 10em">Actions</th>
            </tr>
        </thead>
        <tbody>
        <?php if (isset($user_papers) && is_array($user_papers) && count($user_papers)) : ?>
            <?php foreach ($user_papers as $paper)  :?>
            <tr>
                <td><?php e($paper->title) ?></td>
                <td>
                    <a href="<?= site_url('manage/users/unlink_paper/'. $paper->id .'/'. $user_id) ?>" class="ajax">Remove</a>
                </td>
            </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="2">
                    This user does not have access to any papers at this time.
                </td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
<?= form_close(); ?>
</div>

<?php if ($this->input->is_ajax_request()) : ?>
<script>
    $(document).foundation();
</script>
<?php endif; ?>