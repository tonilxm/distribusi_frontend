<header>
    <div class="row">
        <div class="twelve columns">
            <h2>Recommend Someone to Join</h2>
        </div>
    </div>
</header>

<?php echo form_open( current_url(), 'class="custom"'); ?>

<div class="row">
    <div class="large-9">

        <?php if (validation_errors()) : ?>
        <div class="alert-box alert">
            <?= validation_errors(); ?>
        </div>
        <?php endif; ?>

        <!-- Topic -->
        <div class="row">
            <div class="large-3 columns">
                <label for="topic_id" class="right inline">Topic</label>
            </div>
            <div class="large-9 columns">
                <select name="topic_id">
                <?php if ( ! isset($topics)) : ?>
                    <option value="">No Topics Available.</option>
                <?php else: ?>
                    <option value="">Select a Topic...</option>
                    <?php foreach ($topics as $topic) : ?>
                        <option value="<?php echo $topic->id ?>" <?php echo set_select('topic_id', $topic->id); ?>>
                            <?php e($topic->title); ?>
                        </option>
                    <?php endforeach; ?>
                <?php endif; ?>
                </select>
            </div>
        </div>

        <!-- Name -->
        <div class="row">
            <?php $error = form_error('name') ? 'error' : ''; ?>
            <div class="large-3 columns">
                <label for="name" class="right inline <?= $error ?>">Name</label>
            </div>
            <div class="large-9 columns">
                <input type="text" name="name" class="<?= $error ?>" value="<?= set_value('name'); ?>" />
            </div>
        </div>

        <!-- Job -->
        <div class="row">
            <?php $error = form_error('job') ? 'error' : ''; ?>
            <div class="large-3 columns">
                <label for="job" class="right inline <?= $error ?>">Job/Expertise in Area</label>
            </div>
            <div class="large-9 columns">
                <input type="text" name="job" class="<?= $error ?>" value="<?= set_value('job'); ?>" />
            </div>
        </div>

        <!-- Email -->
        <div class="row">
            <?php $error = form_error('email') ? 'error' : ''; ?>
            <div class="large-3 columns">
                <label for="email" class="right inline <?= $error ?>">Email</label>
            </div>
            <div class="large-9 columns">
                <input type="email" name="email" class="<?= $error ?>" value="<?= set_value('email'); ?>" />
            </div>
        </div>


        <div class="row">
            <div class="large-9 large-offset-3 text-right">
                <input type="submit" name="submit" class="button" value="Recommend" /> &nbsp; or &nbsp;
                <a href="<?= site_url('dashboard'); ?>">Cancel</a>
            </div>
        </div>
    </div>
</div>

<?php echo form_close(); ?>