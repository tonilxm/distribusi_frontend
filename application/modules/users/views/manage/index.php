<div class="row">
<a href="<?= site_url('manage/users/create') ?>" class="create_new">Create New User</a>

<?php if (isset($users) && is_array($users) && count($users)) : ?>

    <table style="width: 100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Username</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Last Login</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user) : ?>
            <tr>
                <td><?= $user->id ?></td>
                <td>
                    <a href="<?= site_url('manage/users/edit/') .'/'. $user->id ?>">
                        <?= $user->username ?>
                    </a>
                    <?php if ($user->banned) : ?>
                        <span class="label alert round">Banned</span>
                    <?php endif; ?>
                </td>
                <td><?= $user->name ?></td>
                <td><?= $user->email ?></td>
                <td><?= $roles[$user->role_id] ?></td>
                <td><?= $user->last_login ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php else: ?>

    <p>No users found.</p>

<?php endif; ?>

</div>