<header>
    <div class="row">
        <div class="twelve columns">
            <h2>Moderation Queue</h2>
        </div>
    </div>
</header>

<div class="row">
<?php if ( isset($users) && is_array($users) && count($users)) : ?>
    <table style="width: 100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Username</th>
                <th>Email</th>
                <th>Name</th>
                <th>Paper</th>
                <th>Signed Up</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user) : ?>
            <tr>
                <td><?= $user->id ?></td>
                <td><a href="<?= site_url('/manage/users/edit/'. $user->id) ?>"><?= $user->username ?></a></td>
                <td><a href="mailto://<?= $user->email ?>"><?= $user->email ?></a></td>
                <td><?= $user->name ?></td>
                <td><?= 'Coming'; ?></td>
                <td><?= date('D, M. jS H:i a') ?></td>
                <td>
                    <a href="<?= site_url('/manage/users/moderate_approve/'. $user->id) ?>" class="button tiny">Approve</a>
                    <a href="<?= site_url('/manage/users/moderate_deny/'. $user->id) ?>" class="button alert tiny">Deny</a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php else: ?>

    <div class="alert-box secondary">
        <span class="icon"></span>
        There are no users awaiting approval.
    </div>

<?php endif; ?>
</div>