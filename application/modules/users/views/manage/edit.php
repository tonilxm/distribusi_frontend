<div class="row">
    <h3>Basic Info</h3>

    <?php if ($user->banned == 1) : ?>
    <div class="alert-box secondary">
        <span class="icon"></span>
        <p>This user is currently Banned.</p>
    </div>
    <?php endif; ?>

    <?php echo form_open( current_url(), 'class="custom"'); ?>

    <?php if (validation_errors()) : ?>
        <div class="alert-box alert">
            <span class="icon"></span>
            <?php echo validation_errors(); ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="large-3 columns">
            <label for="name" class="right inline">Name</label>
        </div>
        <div class="large-9 columns">
            <input type="text" name="name" value="<?php echo set_value('name', $user->name); ?>">
        </div>
    </div>

    <div class="row">
        <div class="large-3 columns">
            <label for="username" class="right inline">Username</label>
        </div>
        <div class="large-9 columns">
            <input type="text" name="username" value="<?php echo set_value('username', $user->username); ?>">
        </div>
    </div>

    <div class="row">
        <div class="large-3 columns">
            <label for="email" class="right inline">Email Address</label>
        </div>
        <div class="large-9 columns">
            <input type="email" name="email" value="<?php echo set_value('email', $user->email); ?>">
        </div>
    </div>

    <div class="row">
        <div class="large-3 columns">
            <label for="work_email" class="right inline">Work Email Address</label>
        </div>
        <div class="large-9 columns">
            <input type="email" name="work_email" value="<?php echo set_value('work_email', $user->work_email); ?>">
        </div>
    </div>

    <div class="row">
        <div class="large-3 columns">
            <label for="password" class="right inline">Password</label>
        </div>
        <div class="large-9 columns">
            <input type="password" name="password" value="">
        </div>
    </div>

    <div class="row">
        <div class="large-3 columns">
            <label for="pass_confirm" class="right inline">Password (Again)</label>
        </div>
        <div class="large-9 columns">
            <input type="password" name="pass_confirm" value="">
        </div>
    </div>


    <hr>

    <div class="row">
        <div class="large-3 columns">
            <label for="role_id" class="right align">Role</label>
        </div>
        <div class="large-9 columns">
            <select name="role_id">
            <?php $cur_role_id = $this->auth->current_user()->role_id; ?>
            <?php foreach ($roles as $role_id => $role_name) : ?>
                <?php if ($role_id <= $cur_role_id) : ?>
                    <option value="<?= $role_id ?>" <?php if ($user->role_id == $role_id) echo 'selected="selected"'; ?>><?= $role_name ?></option>
                <?php endif; ?>
            <?php endforeach; ?>
            </select>
        </div>
    </div>

    <hr/>


    <div class="row">
        <div class="large-3 columns">
            <label for="background" class="right inline">Short Background</label>
        </div>
        <div class="large-9 columns">
            <?php $background = isset($profile->background) ? $profile->background : ''; ?>
            <textarea name="background" style="height: 6em"><?= set_value('background', $background) ?></textarea>
        </div>
    </div>

    <div class="row">
        <div class="large-3 columns">
            <label for="changes" class="right inline">What would they change?</label>
        </div>
        <div class="large-9 columns">
            <?php $changes = isset($profile->changes) ? $profile->changes : ''; ?>
            <textarea name="changes" style="height: 10em"><?= set_value('changes', $changes) ?></textarea>
        </div>
    </div>


    <div class="row">
        <div class="large-3 columns"></div>
        <div class="large-9 columns">
            <input type="submit" name="submit" class="button" value="Save User" /> &nbsp; or &nbsp;
            <a href="<?= site_url('manage/users') ?>">Cancel</a>
        </div>
    </div>

    <?php echo form_close(); ?>


    <br/>


    <hr>

    <h3>Papers</h3>

    <?= $paper_form; ?>

    <hr>

    <h3>Actions </h3>

    <div class="row actions">
        <div class="large-3 columns">
            <label class="right inline">Ban User</label>
        </div>
        <div class="large-9 columns">
        <?php if ($user->banned) : ?>
            <a href="<?= site_url('manage/users/unban/'. $user->id) ?>" onclick="return confirm('UN-Ban this user from the site?');" class="button secondary">Un-Ban User</a>
            <div class="alert-box info">
                <span class="icon"></span>
                <p class="small">This will allow the user to log back in and access the full site.</p>
            </div>
        <?php else: ?>
            <a href="<?= site_url('manage/users/ban/'. $user->id) ?>" onclick="return confirm('Ban this user from the site?');" class="button secondary">Ban User</a>
            <div class="alert-box info">
                <span class="icon"></span>
                <p class="small">This will prevent the user from logging in and accessing the portions of the site restricted to registered users.</p>
            </div>
        <?php endif; ?>
        </div>
    </div>


    <div class="row actions">
        <div class="large-3 columns">
            <label class="right inline">Delete User</label>
        </div>
        <div class="large-9 columns">
            <a href="<?= site_url('manage/users/delete/'. $user->id) ?>" onclick="return confirm('Really delete this user?');" class="button alert">Delete User</a>
            <div class="alert-box warning">
                <span class="icon"></span>
                <p class="small">This is permanent. There is no bringing a user back after this action.</p>
            </div>
        </div>
    </div>
</div>
