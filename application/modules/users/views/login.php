<br/>

<div class="row">
    <div class="large-4 large-offset-4">
        <h1>Login</h1>

        <?php if (validation_errors()) :?>
            <div class="alert-box alert">
                <span class="icon"></span>
                <?php echo validation_errors(); ?>
            </div>
        <?php endif; ?>

        <?php echo form_open(); ?>

            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />

            <label for="username">Username</label>
            <input type="text" name="username" value="<?php echo set_value('username'); ?>" />

            <label for="password">Password</label>
            <input type="password" name="password" value="<?php echo set_value('password'); ?>" />

            <input type="submit" name="submit" class="button" value="Login" />

        <?php echo form_close(); ?>

        <p>
            <a href="<?= site_url('/forgot_password') ?>">Forgot Your Password?</a> <br/>
            Already a member? <a href="<?= site_url('/login') ?>">Sign In</a>.
        </p>

    </div>
</div>

<br/>