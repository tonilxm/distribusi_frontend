<div class="header">
    <div class="row">
        <h1>My Bookmarks</h1>    
    </div>
</div>

        
<?php if (isset($bookmarks) && is_array($bookmarks) && count($bookmarks)) : ?>
    <?php foreach ($bookmarks as $paper_title => $paper) : ?>

<!-- Chapters -->
        <?php foreach ($paper['chapters'] as $chapter) : ?>
           <?php if (isset($chapter['posts']) && is_array($chapter['posts']) && count($chapter['posts'])) : ?>
        <div class="row">
            <div class="large-12 columns no_left">
                <a href="<?= site_url('papers/view_chapter/'. $paper['id'] .'/'. $chapter['id']) ?>">
                    <h2 class="title"><?= $paper_title ?></h2>
                    <div class="row">
                        <div class="large-12 columns">
                            <h3><?= $chapter['title'] ?></h3>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        
        <div class="bookmarks">
            <?php foreach ($chapter['posts'] as $post) : ?>

            <div class="row book post">
                <!-- Post Side -->
                <div class="large-10 columns">

                    <div class="row">
                        <div class="large-11 columns">

                            <!-- Post Header -->
                            <div class="row post-header">
                                <div class="large-6 username">
                                    <span class="user">Username <?= date('g:ia j F Y', strtotime($post->created_on)) ?></span>
                                </div>
                                <div class="large-3 ref">
                                    <a href="<?= site_url('papers/view_references/'. $post->paper_id .'/post/'. $post->post_id) ?>" class="ref-view-toggle">Reference View</a>
                                </div>
                                <div class="large-3 bookmark">
                                    <?= has_bookmarked($post->post_id, 'post', $this->auth->user_id(), $post->paper_id) ?>
                                </div>
                            </div>

                            <!-- Post Body -->
                            <div class="row post-body">
                                <div>
                                    <div class="row post-title">
                                        <? e($post->title) ?>title
                                        <a href="#" class="summary-expander right small" data-id="post-body-<?= $post->post_id ?>" data-show="Expand" data-hide="Hide">Expand</a>
                                
                                    </div>
                                    <div id="post-body-<?= $post->post_id ?>" style="display: none" class="body row">
                                        <?php echo auto_typography($post->body); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row post-footer"></div>

                        </div>

                        <!-- Voting -->
                        <div class="large-1 columns">
                            <?= display_rating($post->post_id, 'post'); ?>
                        </div>
                    </div>

                </div>

                <!-- Action Side -->
                <div class="large-2 columns">
                    <a class="remove-bookmark" href="<?= site_url('bookmarks/remove/post/'. $post->post_id .'/'. $post->bookmark_author); ?>">Remove Bookmark</a>
                </div>

            </div>
            <?php endforeach; ?>
        <?php endif; ?>
        </div>

        <?php endforeach; ?>

    <?php endforeach; ?>

<?php else: ?>
    <div class="alert-box info">
        <span class="icon"></span>
        No bookmarks found.
    </div>
<?php endif; ?>