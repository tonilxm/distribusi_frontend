<h1>Reset Your Password</h1>


<?php if (validation_errors()) : ?>
<div class="alert-box alert">
    <span class="icon"></span>
    <?php echo validation_errors(); ?>
</div>
<?php endif; ?>


<?php if ($can_reset === true) : ?>
<div class="row">
    <div class="small-6 columns">

        <?php echo form_open( current_url() ); ?>

        <input type="hidden" name="code" value="<?= $code ?>">
        <input type="hidden" name="email" value="<?= $email ?>">

        <div class="row">
            <div class="small-4 columns">
                <label for="password" class="right inline">New Password</label>
            </div>
            <div class="small-8 columns">
                <input type="password" name="password" value="" />
            </div>
        </div>

        <div class="row">
            <div class="small-4 columns">
                <label for="pass_confirm" class="right inline">New Password (again)</label>
            </div>
            <div class="small-8 columns">
                <input type="password" name="pass_confirm" value="" />
            </div>
        </div>

        <div class="row">
            <div class="small-4 columns"></div>
            <div class="small-8 columns">
                <input type="submit" name="submit" class="button" value="Reset Password" />
            </div>
        </div>

        <?php echo form_close(); ?>

    </div>

    <div class="small-6 columns text-center"></div>
</div>
<?php else: ?>

    <?php if ( empty($code)) : ?>
        <div class="alert-box info">
            <p>No Verification code was provided. Please check your email again and copy/paste the entire URL in the address bar to try again.</p>
        </div>
    <?php endif; ?>

    <?php if ( ! empty($code) &&  empty($email)) : ?>
        <div class="alert-box info">
            <p>We were unable to match up an account with the Verification code provided. Please check your email again and copy/paste the entire URL in the address bar to try again.</p>
        </div>
    <?php endif; ?>

<?php endif; ?>