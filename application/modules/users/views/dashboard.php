<div class="header">
    <div class="row">
        <h1>Dashboard</h1>
    </div>    
</div>

<div class="row">
    <!-- Left -->
    <div class="large-6 columns pad-right">
        <div class="row">
            <div class="panel">
                <h3>Recommend someone join</h3>
                <p>Know another like minded person? Someone who has ideas about how they would change their area of work, then recommend them to join.</p>
                <a href="<?= site_url('recommend') ?>" class="green button recommend">Recommend</a>
            </div>
        </div>
        <div class="row">
            <a href="" class="primary button large-sub">
                <h3>WRITE A PAPER<span>- Start the discussion - </span></h3>
            </a>
        </div>
        <div class="row">
            <div class="panel">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam. </p> 
                <p>Quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum</p>
            </div>
        </div>
    </div>

    <!-- Right -->
    <div class="large-6 columns pad-left">
        <div class="row">
            <div class="panel">
                <h3>My Profile</h3>
                <?php if ( ! $current_user->verified) : ?>
                    <p>Verify your identity to convert to full membership and gain access to the entire PIN site.</p>
                    <a href="<?= site_url('/profile') ?>" class="blue button verify">VERIFY NOW</a>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="panel">
                <h3>How does the discussion work?</h3>
                <iframe id="panel_video" src="//www.youtube.com/embed/utV1sdjr4PY" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <h2>Your Topics</h2>
    <?php if (isset($papers) && is_array($papers) && count($papers)) : ?>
    <div class="dashboard-papers">
        <?php foreach ($papers as $paper) : ?>
        <div class="paper">
            <div class="large-10 columns post">
                <div class="row paper_header">
                    <?= $paper->title ?>
                </div>
                <div class="row paper_body">
                    <?php echo auto_typography($paper->summary); ?>
                </div>
            </div>
            <div class="large-2 columns">
                <a href="<?= site_url('papers/overview/'. $paper->id) ?>" class="go_to"><span class="icon"></span>GO TO <br >PAPER</a>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<?php else: ?>
    <div class="alert-box secondary">
        <span class="icon"></span>
        You are not linked with any Papers.
    </div>
<?php endif; ?>