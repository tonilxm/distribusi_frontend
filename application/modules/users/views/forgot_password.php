<h1>Forgot Your Password?</h1>

<p>No problem. Enter your email address below and we will send an email to your personal account with a link to reset your password.</p>

<hr>

<?php if (isset($errors) && ! empty($errors)) : ?>
<div class="alert-box alert">
    <span class="icon"></span>
    <?php echo $errors; ?>
</div>
<?php endif; ?>



<div class="row">
    <div class="small-6 columns">

        <?php if (! isset($email_sent) || (isset($email_sent) && $email_sent == false)) : ?>

            <?php echo form_open( current_url() ); ?>

            <!-- Email -->
            <div class="row">
                <div class="small-4 columns">
                    <label for="email" class="right inline">Your Email</label>
                </div>
                <div class="small-8 columns">
                    <input type="text" name="email" class="" value="" />
                </div>
            </div>

            <div class="row">
                <div class="small-4 columns"></div>
                <div class="small-8 columns">
                    <input type="submit" name="submit" class="button" value="Send Email" />
                </div>
            </div>

            <?php echo form_close(); ?>

        <?php elseif ($email_sent === true) : ?>

            <div class="alert-box success">
                <p>An email has been sent to you with instructions to reset your password.</p>
            </div>

        <?php endif; ?>

        <p class="small text-right"><a href="<?= site_url('forgot_password') ?>">Forgot your password?</a></p>
    </div>

    <div class="small-6 columns text-center">



    </div>
</div>