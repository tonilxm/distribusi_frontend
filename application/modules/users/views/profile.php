<div class="header">
    <div class="row">
        <h1>My Profile</h1>
    </div>
</div>
<div class="row">
    <!-- VERIFICATION -->

    <?php if ( ! $user->verified ) : ?>

        <p><b>Verify identity to convert to full membership</b>
        <br/>Only comments by full members will be included in the final report.</p>

        <p class="small">Tell us your name and work email address. We will send a verification code to your work email addrees. Return to the site to use this code to verify your identity.</p>

        <?php echo form_open( current_url(), 'class="custom"'); ?>

        <?php if (validation_errors()) : ?>
        <div class="alert-box alert">
            <span class="icon"></span>
            <?= validation_errors(); ?>
        </div>
        <?php endif; ?>

        <div class="row">
            <div class="small-9">

                <?php $error = form_error('name') ? 'error' : ''; ?>
                <div class="row">
                    <div class="small-3 columns">
                        <label for="name" class="right inline <?= $error ?>">Name</label>
                    </div>
                    <div class="small-9 columns">
                        <input type="text" name="name" class="<?= $error ?>" value="<?= set_value('name', $user->name); ?>" />
                    </div>
                </div>

                <?php $error = form_error('work_email') ? 'error' : ''; ?>
                <div class="row">
                    <div class="small-3 columns">
                        <label for="work_email" class="right inline <?= $error ?>">Work Email</label>
                    </div>
                    <div class="small-9 columns">
                        <input type="email" name="work_email" class="<?= $error ?>" value="<?= set_value('work_email', $user->work_email); ?>" />
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns small-offset-9">
                        <input type="submit" name="submit_send_code" class="button" style="width: 100%" value="Send Code" />
                    </div>
                </div>

            </div>
        </div>
        <?php echo form_close(); ?>

        <?php if ( ! empty($user->activation_code)) : ?>

            <?php echo form_open( current_url(), 'class="custom"' ); ?>
            <div class="row">
                <div class="small-9">

                    <?php $error = form_error('code') ? 'error' : ''; ?>
                    <div class="row">
                        <div class="small-3 columns">
                            <label for="code" class="right inline <?= $error ?>">Code</label>
                        </div>

                        <div class="small-9 columns">
                            <input type="text" name="code" class="<?= $error ?>" value="<?= set_value('code'); ?>" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="small-3 small-offset-9 columns">
                            <input type="submit" name="submit_code" class="button" style="width: 100%" value="Verify Account">
                        </div>
                    </div>
                </div>
            </div>

            <?php echo form_close(); ?>

        <?php endif; ?>

        <hr/>
    <?php endif; ?>

    <!-- TOPICS -->

    <h3>Papers you are registered for</h3>

    <?php if (isset($papers) && is_array($papers) && count($papers)) : ?>
        <table style="width: 100%">
            <thead>
                <tr>
                    <th>Title</th>
                    <th style="width: 6em">Discussions</th>
                    <th>Latest Post On</th>
                    <th># Bookmarks</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($papers as $paper) : ?>
                <tr>
                    <td>
                        <a href="<?= site_url('papers/view/'. $paper->id) ?>">
                            <?= $paper->title ?>
                        </a>
                    </td>
                    <td clas="text-center"><?= (int)$paper->post_count + (int)$paper->comment_count ?></td>
                    <td><?= date('g:ia j F Y', strtotime($paper->last_post_date)) ?></td>
                    <td>
                    <?php
                        $count = user_has_bookmarks($paper->id);
                        if ($count)
                        {
                            echo "<a href='". site_url('users/bookmarks/'. $paper->id) ."'>{$count}</a>";
                        }
                    ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else : ?>

        <div class="alert-box secondary">
            You are not currently registered to any Papers.
        </div>

    <?php endif; ?>

    <hr>

    <!-- PROFILE -->

    <h3>Edit Your Profile</h3>

    <?php echo form_open( current_url(), 'class="custom"' ); ?>

    <div class="row">
        <div class="small-9">

            <?php $error = form_error('email') ? 'error' : ''; ?>
            <div class="row">
                <div class="small-3 columns">
                    <label for="email" class="right inline <?= $error ?>">Email for Correspondance</label>
                </div>

                <div class="small-9 columns">
                    <input type="email" name="email" class="<?= $error ?>" value="<?= set_value('email', $user->email); ?>" />
                </div>
            </div>

            <?php $error = form_error('work_email') ? 'error' : ''; ?>
            <div class="row">
                <div class="small-3 columns">
                    <label for="work_email" class="right inline <?= $error ?>">Work Email</label>
                </div>

                <div class="small-9 columns">
                    <input type="email" name="work_email" class="<?= $error ?>" value="<?= set_value('work_email', $user->work_email); ?>" />
                </div>
            </div>

            <!-- Background -->
            <div class="row">
                <?php $error = form_error('background') ? 'error' : ''; ?>
                <div class="small-3 columns">
                    <label class="right <?php echo $error ?>"><b>Short Background</b><br/>Keep it vague so that you cannot be identified by this.</label>
                </div>
                <div class="small-9 columns">
                    <textarea name="background" style="height: 6.5em" class="<?php echo $error ?>"><?php echo set_value('background', $user->background) ?></textarea>
                </div>
            </div>

            <!-- Changes -->
            <div class="row">
                <?php $error = form_error('changes') ? 'error' : ''; ?>
                <div class="small-3 columns">
                    <label class="right <?php echo $error ?>"><b>What would you change?</b><br/>If there was one thing you could change in your industry, what would it be?</label>
                </div>
                <div class="small-9 columns">
                    <textarea name="changes" style="height: 10.5em" class="<?php echo $error ?>"><?php echo set_value('changes', $user->changes) ?></textarea>
                </div>
            </div>



            <div class="row">
                <div class="small-9 small-offset-3 columns">
                    <input type="submit" name="submit_profile" class="button" value="Update Account">
                </div>
            </div>
        </div>
    </div>

    <?php echo form_close(); ?>
    </div>