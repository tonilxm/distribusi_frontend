<div class="header">
    <div class="row">
        <h1>Join the Policy &amp; Ideas Network</h1>
    </div>
</div>

<div class="row">
<p>Text on anonymity is important, etc...</p>

<p class="text-right">
    Already a member? <a href="<?= site_url('/login') ?>">Sign In</a>.
</p>

<div class="register">

    <?php if (validation_errors()) :?>
        <div class="alert-box alert">
            <span class="icon"></span>
            <?php echo validation_errors(); ?>
        </div>
    <?php endif; ?>

    <form action="" method="post" class="custom">

    <div class="row">
        <div class="small-12">

            <!-- Topic -->
            <div class="row">
                <div class="small-3 columns">
                    <label for="paper_id" class="right inline">Paper you wish to join</label>
                </div>
                <div class="small-9 columns">
                    <select name="paper_id">
                        <?php if ( ! isset($papers)) : ?>
                            <option value="">No Papers Available.</option>
                        <?php else: ?>
                            <option value="">Select a Paper...</option>
                            <?php foreach ($papers as $paper) : ?>
                                <option value="<?= $paper->id ?>" <?php echo set_select('paper_name', $paper->id); ?>>
                                    <?php e($paper->title); ?>
                                </option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>

            <!-- Username -->
            <?php $error = form_error('username') ? 'error' : ''; ?>
            <div class="row">
                <div class="small-3 columns">
                    <label for="username" class="right inline <?php echo $error ?>">Username</label>
                </div>
                <div class="small-9 columns">
                    <input type="text" name="username" class="<?php echo $error ?>" value="<?php echo set_value('username') ?>"  />
                </div>
            </div>

            <!-- Email -->
            <?php $error = form_error('email') ? 'error' : ''; ?>
            <div class="row">
                <div class="small-3 columns">
                    <label for="email" class="right inline <?php echo $error ?>">Email Address</label>
                </div>
                <div class="small-9 columns">
                    <input type="email" name="email" class="<?php echo $error ?>" value="<?php echo set_value('email') ?>"  />
                </div>
            </div>

            <!-- Password -->
            <?php $error = form_error('password') ? 'error' : ''; ?>
            <div class="row">
                <div class="small-3 columns">
                    <label for="password" class="right inline <?php echo $error ?>">Password</label>
                </div>
                <div class="small-9 columns">
                    <input type="password" name="password" class="<?php echo $error ?>" value="<?php echo set_value('password') ?>"  />
                    <p class="small">Min. 8 characters. Max. 72.</p>
                </div>
            </div>

            <!-- Pass Confirm -->
            <?php $error = form_error('pass_confirm') ? 'error' : ''; ?>
            <div class="row">
                <div class="small-3 columns">
                    <label for="pass_confirm" class="right inline <?php echo $error ?>">Password (again)</label>
                </div>
                <div class="small-9 columns">
                    <input type="password" name="pass_confirm" class="<?php echo $error ?>" value="<?php echo set_value('pass_confirm') ?>"  />
                </div>
            </div>

            <!-- Background -->
            <div class="row">
                <?php $error = form_error('background') ? 'error' : ''; ?>
                <div class="small-3 columns">
                    <label class="right <?php echo $error ?>"><b>Short Background</b><br/>Keep it vague so that you cannot be identified by this.</label>
                </div>
                <div class="small-9 columns">
                    <textarea name="background" style="height: 6.5em" class="<?php echo $error ?>"><?php echo set_value('background') ?></textarea>
                </div>
            </div>

            <!-- Changes -->
            <div class="row">
                <?php $error = form_error('changes') ? 'error' : ''; ?>
                <div class="small-3 columns">
                    <label class="right <?php echo $error ?>"><b>What would you change?</b><br/>If there was one thing you could change in your industry, what would it be?</label>
                </div>
                <div class="small-9 columns">
                    <textarea name="changes" style="height: 10.5em" class="<?php echo $error ?>"><?php echo set_value('changes') ?></textarea>
                </div>
            </div>
        </div>
    </div>

    <div class="row text-right">
        <input type="submit" name="submit" class="button" value="Submit Application" />
    </div>

    </form>

</div>
</div>