
$(document).ready(function() {
    var table =
        $('.datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 15,
            "fnDrawCallback": function( oSettings ) {
                // if .sidebar-pf exists, call sidebar() after the data table is drawn
                if ($('.sidebar-pf').length > 0) {
                    $(document).sidebar();
                }
            },
            "ajax":  {
                "url": 'principal/search'
            },
            "columnDefs": [
                {
                    "targets" : [0],
                    "data": null,
                    "render" : {
                        "_": "name",
                        "filter": "name",
                        "display": function(data, type, row, meta) {
                            return "<a href='principal/"+data.dt_RowId+"'>"+data.name+"</a>";
                        }
                    }
                },
                {
                    "targets" : [1],
                    "data": "address"
                },
                {
                    "targets" : [2],
                    "data": "city"
                },
                {
                    "targets" : [3],
                    "data": "pic"
                }
            ]
        });

    // Event listener to the two range filtering inputs to redraw on input
    $('#btnSearch').click( function() {
        var pName = $('#principal_name').val();
        var pAddress = $('#principal_city_address').val();
        table.column(0).search(pName)
            .column(1).search(pAddress).draw();
    } );

    $("#btnClearAll").click(function(e) {
        e.preventDefault();
        $('#principal_name').val('');
        $('#principal_city_address').val('');
    });
});