
$(document).ready(function() {
    var table =
        $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 15,
            fnDrawCallback: function( oSettings ) {
                // if .sidebar-pf exists, call sidebar() after the data table is drawn
                if ($('.sidebar-pf').length > 0) {
                    $(document).sidebar();
                }
            },
            ajax:  {
                url: 'outlet/search'
            },
            columnDefs: [
                {
                    "targets" : [0],
                    "data": null,
                    "render" : {
                        "_": "name",
                        "filter": "name",
                        "display": function(data, type, row, meta) {
                            return "<a href='outlet/"+data.dt_RowId+"'>"+data.name+"</a>";
                        }
                    }
                },
                {
                    targets : [1],
                    data: "address"
                },
                {
                    targets : [2],
                    data: "areaName"
                }
            ]
        });

    // Event listener to the two range filtering inputs to redraw on input
    $('#btnSearch').click( function() {
        var pCode = $('#area_code').val();
        var pName = $('#area_name').val();
        table.column(0).search(pCode).column(1).search(pName).draw();
    } );

    $("#btnClearAll").click(function(e) {
        e.preventDefault();
        $('#outlet_code').val('');
        $('#outlet_name').val('');
    });
});