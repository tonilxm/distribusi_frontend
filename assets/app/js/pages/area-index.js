
$(document).ready(function() {
    var table =
        $('.datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 15,
            "fnDrawCallback": function( oSettings ) {
                // if .sidebar-pf exists, call sidebar() after the data table is drawn
                if ($('.sidebar-pf').length > 0) {
                    $(document).sidebar();
                }
            },
            "ajax":  {
                "url": 'area/search'
            },
            "columnDefs": [
                {
                    "targets" : [0],
                    "data": null,
                    "render" : {
                        "_": "code",
                        "filter": "code",
                        "display": function(data, type, row, meta) {
                            return "<a href='area/"+data.dt_RowId+"'>"+data.code+"</a>";
                        }
                    }
                },
                {
                    "targets" : [1],
                    "data": "name"
                }
            ]
        });

    // Event listener to the two range filtering inputs to redraw on input
    $('#btnSearch').click( function() {
        var pCode = $('#area_code').val();
        var pName = $('#area_name').val();
        table.column(0).search(pCode).column(1).search(pName).draw();
    } );

    $("#btnClearAll").click(function(e) {
        e.preventDefault();
        $('#area_code').val('');
        $('#area_name').val('');
    });
});