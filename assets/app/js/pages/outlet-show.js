$(document).ready(function() {
    // init boot strap combobox component!
    $('.combobox').combobox();

    // manage tab
    $("#main_data_lnk").click(function(evt) {
        evt.preventDefault();

        selectTab($(this));

        // display main-data form
        $('.bank-data').hide();
        $('.main-data').show(0);
    });

    $("#bank_data_lnk").click(function(evt) {
        evt.preventDefault();

        selectTab($(this));

        // display main-data form
        $('.main-data').hide();
        $('.bank-data').show(0);

    });

    function selectTab(element) {
        $("ul.nav-tabs > li").each(function() {
            $(this).removeAttr('class');
        });

        element.closest('li').attr('class', 'active');
    }

    $(".bank-data").on('click', '#btn_add_bank', function(e) {
        var rowCount = $('#tbl_bank tr').length;

        e.preventDefault();
        var content = '<tr><td class="text-center row-no">' + rowCount + '</td>' +
            '<td class="bank-acc-no"><input type="text" name="acc_no[]" value="" placeholder="Masukan No. Rekening" size="30"></td>' +
            '<td class="bank-info"><input type="text" name="bank_info[]" value="" placeholder="Masukan Informasi Bank" size="100"></td>' +
            '<td><a href="#" class="btn btn-danger btn-xs remove-bank-row">Hapus</a></td></tr>';

        $('#tbl_bank > tbody:last-child').append(content);
    });

    $("#tbl_bank").on('click', '.remove-bank-row', function(e) {
        e.preventDefault();

        $(this).closest('tr').remove();

        // recompute row no
        $('#tbl_bank tr').each(function(idx) {
            if (idx > 0) {
                $(this).children(':first').text(idx);
            }
        })
    });

    $('#btnDelete').click( function(e) {
        e.preventDefault();
    } );
});