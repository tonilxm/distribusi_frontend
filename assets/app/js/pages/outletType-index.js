
$(document).ready(function() {
    var table =
        $('.datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 15,
            "fnDrawCallback": function( oSettings ) {
                // if .sidebar-pf exists, call sidebar() after the data table is drawn
                if ($('.sidebar-pf').length > 0) {
                    $(document).sidebar();
                }
            },
            "ajax":  {
                "url": 'outletType/search'
            },
            "columnDefs": [
                {
                    "targets" : [0],
                    "data": null,
                    "render" : {
                        "_": "code",
                        "filter": "code",
                        "display": function(data, type, row, meta) {
                            return "<a href='outletType/"+data.dt_RowId+"'>"+data.code+"</a>";
                        }
                    }
                },
                {
                    "targets" : [1],
                    "data": "name"
                }
            ]
        });

    // Event listener to the two range filtering inputs to redraw on input
    $('#btnSearch').click( function() {
        var pCode = $('#outletType_code').val();
        var pName = $('#outletType_name').val();
        table.column(0).search(pCode).column(1).search(pName).draw();
    } );

    $("#btnClearAll").click(function(e) {
        e.preventDefault();
        $('#outletType_code').val('');
        $('#outletType_name').val('');
    });
});