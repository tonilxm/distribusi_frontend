
$(document).ready(function() {
    var table =
        $('.datatable').DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 15,
            "fnDrawCallback": function( oSettings ) {
                // if .sidebar-pf exists, call sidebar() after the data table is drawn
                if ($('.sidebar-pf').length > 0) {
                    $(document).sidebar();
                }
            },
            "ajax":  {
                "url": 'brand/search'
            },
            "columnDefs": [
                {
                    "targets" : [0],
                    "data": null,
                    "render" : {
                        "_": "code",
                        "filter": "code",
                        "display": function(data, type, row, meta) {
                            return "<a href='brand/"+data.dt_RowId+"'>"+data.code+"</a>";
                        }
                    }
                },
                {
                    "targets" : [1],
                    "data": "name"
                },
                {
                    "targets" : [2],
                    "data": "principal"
                }
            ]
        });

    // Event listener to the two range filtering inputs to redraw on input
    $('#btnSearch').click( function() {
        var pCode = $('#brand_code').val();
        var pName = $('#brand_name').val();
        table.column(0).search(pCode).column(1).search(pName).draw();
    } );

    $("#btnClearAll").click(function(e) {
        e.preventDefault();
        $('#brand_code').val('');
        $('#brand_name').val('');
    });
});